package utils_test

import (
	"net/http"
	"testing"
	"unsafe"

	"github.com/fajarmzky/base-project/pkg/utils"
	"github.com/stretchr/testify/assert"
)

func TestCurrentFuncName(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		// act
		name := utils.CurrentFuncName()

		// assert
		assert.Equal(t, name, "github.com/fajarmzky/base-project/pkg/utils_test.TestCurrentFuncName.func1")
	})
}

func TestUnmarshalToMap(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		// act
		out := utils.UnmarshalToMap([]byte(`{"data":123,"values":{"sub1":"ok"}}`))

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, len(out), 2)
	})

	t.Run("err bad data", func(t *testing.T) {
		// arrange
		// act
		out := utils.UnmarshalToMap([]byte(`{"data":123,"values`))

		// assert
		assert.Empty(t, out)
	})
}

func TestRemarshalToMap(t *testing.T) {
	type Data struct {
		Value1 string `json:"value1"`
		Value2 int    `json:"value2"`
	}

	t.Run("ok", func(t *testing.T) {
		// arrange
		data := Data{Value1: "ok", Value2: 123}

		// act
		out := utils.RemarshalToMap(data)

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, len(out), 2)
	})

	t.Run("err bad data 1", func(t *testing.T) {
		// arrange
		// act
		out := utils.RemarshalToMap("1231")

		// assert
		assert.Empty(t, out)
	})

	t.Run("err bad data 2", func(t *testing.T) {
		// arrange
		// act
		out := utils.RemarshalToMap(unsafe.Pointer(nil))

		// assert
		assert.Empty(t, out)
	})
}

func TestFmtCompactHTTPHeader(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		header := http.Header{}
		header.Set("12311", "1231231")
		header.Set("Abcd", "asdasd")

		// act
		out := utils.FmtCompactHTTPHeader(header)

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, len(out), 2)
	})
}

func TestCensorLargeMap(t *testing.T) {
	type Data struct {
		Value1 string `json:"value1"`
		Value2 int    `json:"value2"`
		Nested *Data  `json:"nested"`
	}

	t.Run("ok", func(t *testing.T) {
		// arrange
		data := Data{
			Value1: "123121312312312123121312312312123121312312312123121312312312",
			Value2: 12356789,
			Nested: &Data{
				Value1: "123121312312312123121312312312123121312312312123121312312312",
				Value2: 123,
				Nested: nil,
			},
		}

		datamap := utils.RemarshalToMap(data)

		// act
		out := utils.CensorMapContentLength(datamap, 5)

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, len(out), len(datamap))
	})
}

func TestCast(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		type A struct {
			Abra    string
			Cadabra string
		}
		type B struct {
			Abra    string
			Cadabra string
		}

		a := A{Abra: "a", Cadabra: "c"}
		b := B{}

		// act
		err := utils.Cast(&a, &b)

		// assert
		assert.NoError(t, err)
		assert.Equal(t, b.Abra, a.Abra)
	})

	t.Run("ok diff type A", func(t *testing.T) {
		// arrange
		type A struct {
			Abra    string
			Cadabra string
		}
		type B struct {
			Abra     string
			Cadabra  string
			Alakazam string
		}

		a := A{Abra: "a", Cadabra: "c"}
		b := B{}

		// act
		err := utils.Cast(&a, &b)

		// assert
		assert.NoError(t, err)
		assert.Equal(t, b.Abra, a.Abra)
	})

	t.Run("ok strict diff type A", func(t *testing.T) {
		// arrange
		type A struct {
			Abra    string
			Cadabra string
		}
		type B struct {
			Abra     string
			Cadabra  string
			Alakazam string
		}

		a := A{Abra: "a", Cadabra: "c"}
		b := B{}

		// act
		err := utils.CastWithOpts(&a, &b, utils.CastOpts{StrictType: true})

		// assert
		assert.NoError(t, err)
		assert.Equal(t, b.Abra, a.Abra)
	})

	t.Run("ok diff type B", func(t *testing.T) {
		// arrange
		type A struct {
			Abra     string
			Cadabra  string
			Alakazam string
		}
		type B struct {
			Abra    string
			Cadabra string
		}

		a := A{Abra: "a", Cadabra: "c", Alakazam: "d"}
		b := B{}

		// act
		err := utils.Cast(&a, &b)

		// assert
		assert.NoError(t, err)
		assert.Equal(t, b.Abra, a.Abra)
	})

	t.Run("err strict diff type B", func(t *testing.T) {
		// arrange
		type A struct {
			Abra     string
			Cadabra  string
			Alakazam string
		}
		type B struct {
			Abra    string
			Cadabra string
		}

		a := A{Abra: "a", Cadabra: "c", Alakazam: "d"}
		b := B{}

		// act
		err := utils.CastWithOpts(&a, &b, utils.CastOpts{StrictType: true})

		// assert
		assert.Error(t, err)
	})

	t.Run("err panic strict diff type B", func(t *testing.T) {
		// arrange
		type A struct {
			Abra     string
			Cadabra  string
			Alakazam string
		}
		type B struct {
			Abra    string
			Cadabra string
		}

		a := A{Abra: "a", Cadabra: "c", Alakazam: "d"}
		b := B{}

		// act
		// assert
		assert.Panics(t, func() {
			utils.CastWithOpts(&a, &b, utils.CastOpts{StrictType: true, PanicOnFail: true})
		})
	})

	t.Run("cov boost err marshal", func(t *testing.T) {
		// arrange
		type A struct {
			Abra     string
			Cadabra  string
			Alakazam chan (int)
		}
		type B struct {
			Abra    string
			Cadabra string
		}

		a := A{Abra: "a", Cadabra: "c", Alakazam: make(chan int)}
		b := B{}

		// act
		err := utils.CastWithOpts(&a, &b, utils.CastOpts{StrictType: true})

		// assert
		assert.Error(t, err)
	})
}
