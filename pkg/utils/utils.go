package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"runtime"
	"strings"
)

// CurrentFuncName get function name where this function called
func CurrentFuncName() (fname string) {
	pc, _, _, ok := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		fname = details.Name()
	}
	return
}

// UnmarshalToMap unmarshal any json encoded bytes to map of interface. Will return empty map on failure.
func UnmarshalToMap(bytes []byte) (m map[string]interface{}) {
	m = map[string]interface{}{}
	err := json.Unmarshal(bytes, &m)
	if err != nil {
		return
	}
	return
}

// RemarshalToMap marshal any object to map of interface. Will return empty map on failure.
func RemarshalToMap(i interface{}) (m map[string]interface{}) {
	m = map[string]interface{}{}
	bytes, err := json.Marshal(i)
	if err != nil {
		return
	}
	err = json.Unmarshal(bytes, &m)
	if err != nil {
		return
	}
	return
}

// FmtCompactHTTPHeader marshal http to map of simple string. Will return empty map on failure.
func FmtCompactHTTPHeader(i http.Header) (m map[string]string) {
	m = map[string]string{}
	for k, v := range i {
		m[k] = strings.Join(v, "; ")
	}
	return
}

// CensorMapContentLength will censor strings contained in map if the string value exceed max chars. Will only censor string value.
func CensorMapContentLength(in map[string]interface{}, maxchars int) (out map[string]interface{}) {
	padLength := 20
	out = CensorMap(in, func(key string, val interface{}) (out interface{}) {
		str, isstring := val.(string)
		if isstring && len(str) > padLength && len(str) > maxchars {
			in[key] = fmt.Sprintf("%s... (censored %d chars) ...%s",
				str[0:padLength/2],
				len(str)-padLength,
				str[len(str)-padLength/2:],
			)
		}
		return str
	})
	return out
}

func CensorMap(in map[string]interface{}, fx func(key string, val interface{}) (out interface{})) (out map[string]interface{}) {
	for key, value := range in {
		if submap, ok := value.(map[string]interface{}); ok {
			in[key] = CensorMap(submap, fx)
			continue
		}
		in[key] = fx(key, value)
	}
	return in
}

type CastOpts struct {
	StrictType  bool
	PanicOnFail bool
}

// Cast try to cast a object to other type
func Cast(in interface{}, out interface{}) (err error) {
	return CastWithOpts(in, out, CastOpts{StrictType: false})
}

// CastWithOpts try to cast a object to other type with options
func CastWithOpts(in interface{}, out interface{}, opts CastOpts) (err error) {
	defer func() {
		if opts.PanicOnFail && err != nil {
			panic(err)
		}
	}()

	bts, err := json.Marshal(in)
	if err != nil {
		err = fmt.Errorf("serialize failure: %w", err)
		return
	}
	dec := json.NewDecoder(bytes.NewBuffer(bts))
	if opts.StrictType {
		dec.DisallowUnknownFields()
	}
	err = dec.Decode(out)
	if err != nil {
		err = fmt.Errorf("deserialize failure: %w", err)
		return
	}
	return
}
