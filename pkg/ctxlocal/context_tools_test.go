package ctxlocal_test

import (
	"context"
	"testing"

	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/fajarmzky/base-project/pkg/ctxlocal"
	"github.com/stretchr/testify/assert"
)

func TestAuthData(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		data := ctxlocal.AuthData{SubjectID: "abc"}

		ctx1 := ctxcontent.CreateWith(context.Background())
		ctx2 := ctxcontent.CreateWith(context.Background())

		// act
		ctxlocal.SetAuthData(ctx1, data)
		out1 := ctxlocal.GetAuthData(ctx1)
		out2 := ctxlocal.GetAuthData(ctx2)

		// assert
		assert.NotEmpty(t, out1)
		assert.Empty(t, out2)
		assert.Equal(t, data.SubjectID, out1.SubjectID)
	})
}
