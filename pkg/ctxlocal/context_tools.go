package ctxlocal

import (
	"context"
	"time"

	"github.com/fajarmzky/base-project/pkg/ctxcontent"
)

var refKeyServiceInfo = "*ctxkey:ctxlocal:serviceinfo"

type ServiceInfo struct {
	ServiceID           string
	InstanceID          string
	InfoVersion         string
	InfoListenedPort    int
	InfoServerStartTime time.Time
	InfoLocalIPAddress  string
}

func SetServiceInfo(ctx context.Context, in ServiceInfo) {
	ctxcontent.SetData(ctx, refKeyServiceInfo, in)
}

func GetServiceInfo(ctx context.Context) (out ServiceInfo) {
	out, _ = ctxcontent.GetData(ctx, refKeyServiceInfo).(ServiceInfo) // intended return empty object
	return
}

// ***

var refKeyAuthData = "*ctxkey:ctxlocal:authdata"

type AuthData struct {
	SubjectID string
}

func SetAuthData(ctx context.Context, in AuthData) {
	ctxcontent.SetData(ctx, refKeyAuthData, in)
}

func GetAuthData(ctx context.Context) (out AuthData) {
	out, _ = ctxcontent.GetData(ctx, refKeyAuthData).(AuthData) // intended return empty object
	return
}
