package ctxcontent

import (
	"context"
	"sync"
)

type mem string

var refkey = mem(".")

//go:generate moq -stub -out mocks_test.go -pkg ctxcontent_test . Authorizer

type Context struct {
	data sync.Map
}

func (c Context) Default() (out Context) {
	return Context{data: sync.Map{}}
}

func CreateWith(in context.Context) (out context.Context) {
	ctx := Context{}.Default()
	out = context.WithValue(in, refkey, &ctx)

	return
}

func ExtractFrom(ctx context.Context) (ctxdata *Context, ok bool) {
	// prevent panic nil if type conversion is not true
	if ctxdata, ok = ctx.Value(refkey).(*Context); !ok {
		ctx := Context{}.Default()
		ctxdata = &ctx
	}
	return
}
