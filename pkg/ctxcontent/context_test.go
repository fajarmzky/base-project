package ctxcontent_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/stretchr/testify/assert"
)

func TestExtractContext(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := context.Background()

		// act
		ctxRequest, ok := ctxcontent.ExtractFrom(ctx)

		// assert
		assert.NotNil(t, ctxRequest)
		assert.False(t, ok)
	})
}

func TestCreateFrom(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := context.Background()

		// act
		ctxRequest := ctxcontent.CreateWith(ctx)

		// assert
		assert.NotNil(t, ctxRequest)
	})
}

func TestAddWarning(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := context.Background()
		ctx = ctxcontent.CreateWith(ctx)

		// act
		ctxcontent.AddWarning(ctx, fmt.Errorf("sample error").Error())
		ctxRequest, _ := ctxcontent.ExtractFrom(ctx)

		// assert
		assert.NotNil(t, ctxRequest)
	})
}

func TestListWarning(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := context.Background()
		ctx = ctxcontent.CreateWith(ctx)

		ctxcontent.AddWarning(ctx, fmt.Errorf("sample error 1").Error())
		ctxcontent.AddWarning(ctx, fmt.Errorf("sample error 2").Error())
		ctxcontent.AddWarning(ctx, fmt.Errorf("sample error 3").Error())

		// act
		errs := ctxcontent.ListWarnings(ctx)

		// assert
		assert.NotNil(t, errs)
		assert.Equal(t, 3, len(errs))
	})
}
