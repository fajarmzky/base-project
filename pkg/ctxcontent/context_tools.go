package ctxcontent

import (
	"context"
	"fmt"
	"strings"

	"github.com/rs/xid"
)

func SetData(ctx context.Context, key string, val interface{}) {
	c, _ := ExtractFrom(ctx)
	c.data.Store(key, val)
}

func GetData(ctx context.Context, key string) (val interface{}) {
	c, _ := ExtractFrom(ctx)
	val, _ = c.data.Load(key)
	return
}

// ***

var prefixKeyWarning = "*ctxkey:core:warning::"

func AddWarning(ctx context.Context, val string) {
	key := prefixKeyWarning + xid.New().String()
	SetData(ctx, key, val)
}

func ListWarnings(ctx context.Context) (out []string) {
	c, _ := ExtractFrom(ctx)
	out = []string{}

	c.data.Range(func(key, val interface{}) bool {
		if strkey, ok := key.(string); !ok || !strings.HasPrefix(strkey, prefixKeyWarning) {
			return true
		}
		if strval, ok := val.(string); ok {
			out = append(out, strval)
		}
		return true
	})

	return
}

// ***

var prefixKeyVars = "*ctxkey:core:vars::"

type kv struct {
	key string
	val interface{}
}

func AddVar(ctx context.Context, varkey string, varval interface{}) {
	key := prefixKeyVars + xid.New().String()
	SetData(ctx, key, kv{key: varkey, val: varval})
}

func ListVars(ctx context.Context) (out map[string]interface{}) {
	out = map[string]interface{}{}

	c, _ := ExtractFrom(ctx)
	c.data.Range(func(key, val interface{}) bool {
		if strkey, ok := key.(string); !ok || !strings.HasPrefix(strkey, prefixKeyVars) {
			return true
		}
		if kvval, ok := val.(kv); ok {
			out[kvval.key] = kvval.val
		}
		return true
	})

	return
}

// ***

var prefixKeyProcess = "*ctxkey:core:process::"

type Process struct {
	Name string
	Data map[string]interface{}
}

func (e *Process) SetData(key string, data interface{}) {
	e.Data[key] = data
}

func CreateProcess(ctx context.Context, proc Process) (out *Process) {
	if proc.Data == nil {
		proc.Data = map[string]interface{}{}
	}

	key := prefixKeyProcess + xid.New().String()
	data := &proc

	SetData(ctx, key, data)

	out = data
	return
}

func ListProcesses(ctx context.Context) (out []*Process) {
	out = []*Process{}

	c, _ := ExtractFrom(ctx)
	c.data.Range(func(key, val interface{}) bool {
		if strkey, ok := key.(string); !ok || !strings.HasPrefix(strkey, prefixKeyProcess) {
			return true
		}
		if kvval, ok := val.(*Process); ok {
			out = append(out, kvval)
		}
		return true
	})

	return
}

func ListProcessesAsMap(ctx context.Context) (out map[string]interface{}) {
	out = map[string]interface{}{}
	processes := ListProcesses(ctx)
	for _, v := range processes {
		out[v.Name] = v.Data
	}
	return
}

// ***

var refKeyAuthAuthorizer = "*ctxkey:core:auth::authorizer"

type Authorizer interface {
	Authorize(ctx context.Context, actions string, resourceID string) (err error)
}

func SetAuthorizer(ctx context.Context, inst Authorizer) {
	SetData(ctx, refKeyAuthAuthorizer, inst)
}

func Authorize(ctx context.Context, action string, resource string) (err error) {
	authr, ok := GetData(ctx, refKeyAuthAuthorizer).(Authorizer)
	if authr == nil || !ok {
		err = fmt.Errorf("bad authorizer: engine nil or not set")
		return
	}
	err = authr.Authorize(ctx, action, resource)
	if err != nil {
		return
	}
	return
}
