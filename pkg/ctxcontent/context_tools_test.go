package ctxcontent_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/stretchr/testify/assert"
)

func TestSetData(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())

		// act
		ctxcontent.SetData(ctx, "a", "a")
		ctxcontent.SetData(ctx, "b", "b")

		// assert
		out, ok := ctxcontent.ExtractFrom(ctx)
		assert.True(t, ok)
		assert.NotEmpty(t, out)
	})
}

func TestGetData(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "a", "a")
		ctxcontent.SetData(ctx, "b", "b")

		// act
		outa := ctxcontent.GetData(ctx, "a")
		outb := ctxcontent.GetData(ctx, "b")

		// assert
		assert.Equal(t, outa, "a")
		assert.Equal(t, outb, "b")
	})
}

func TestAddWarningListWarnings(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		err := fmt.Errorf("random error")

		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution

		// act
		ctxcontent.AddWarning(ctx, err.Error())
		ctxcontent.AddWarning(ctx, err.Error())
		out := ctxcontent.ListWarnings(ctx)

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, 2, len(out))
	})

	t.Run("ok empty context", func(t *testing.T) {
		// arrange
		ctx := context.Background()
		err := fmt.Errorf("random error")

		// act
		ctxcontent.AddWarning(ctx, err.Error())
		out := ctxcontent.ListWarnings(ctx)

		// assert
		assert.Empty(t, out)
	})
}

func TestAddVarAndListVars(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution

		// act
		ctxcontent.AddVar(ctx, "k1", "v1")
		ctxcontent.AddVar(ctx, "k2", "v2")
		out := ctxcontent.ListVars(ctx)

		// assert
		assert.NotEmpty(t, out)
		assert.Equal(t, 2, len(out))
		assert.Equal(t, "v1", out["k1"])
	})
}

func TestAddProcessAndListProcessesAndListProcessesAsMap(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution
		ctxcontent.AddVar(ctx, "k1", "v1")        // add pollution

		// act
		proc := ctxcontent.CreateProcess(ctx, ctxcontent.Process{Name: "outgoing-firebase"})
		proc.SetData("input.id", "1234")
		proc.SetData("input.target", "@resource")
		proc.SetData("output.status", "200")
		proc.SetData("output.message", "finished success")

		out := ctxcontent.ListProcesses(ctx)
		outmap := ctxcontent.ListProcessesAsMap(ctx)

		// assert
		assert.NotEmpty(t, out)
		assert.NotEmpty(t, outmap)
		assert.Equal(t, "200", out[0].Data["output.status"])
	})
}

func TestSetAuthorizer(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution
		ctxcontent.AddVar(ctx, "k1", "v1")        // add pollution

		// act
		ctxcontent.SetAuthorizer(ctx, &AuthorizerMock{
			AuthorizeFunc: func(ctx context.Context, actions string, resourceID string) error { return nil },
		})
		err := ctxcontent.Authorize(ctx, "read", "file:x")

		// assert
		assert.NoError(t, err)
	})

	t.Run("err authorization failure", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution
		ctxcontent.AddVar(ctx, "k1", "v1")        // add pollution

		// act
		ctxcontent.SetAuthorizer(ctx, &AuthorizerMock{
			AuthorizeFunc: func(ctx context.Context, actions string, resourceID string) error { return fmt.Errorf("auth failed") },
		})
		err := ctxcontent.Authorize(ctx, "read", "file:x")

		// assert
		assert.Error(t, err)
	})

	t.Run("err empty authorizer", func(t *testing.T) {
		// arrange
		ctx := ctxcontent.CreateWith(context.Background())
		ctxcontent.SetData(ctx, "abcd", "lalala") // add pollution
		ctxcontent.AddVar(ctx, "k1", "v1")        // add pollution

		// act
		err := ctxcontent.Authorize(ctx, "read", "file:x")

		// assert
		assert.Error(t, err)
	})
}
