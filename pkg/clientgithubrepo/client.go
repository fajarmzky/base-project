package clientgithubrepo

import (
	"context"
	"fmt"
)

var (
	ErrResiliencyFailure = fmt.Errorf("resiliency failure")
	ErrConnectionProblem = fmt.Errorf("connection problem")
)

type Client interface {
	GetPopularRepoNames(ctx context.Context) (names []string, err error)
}
