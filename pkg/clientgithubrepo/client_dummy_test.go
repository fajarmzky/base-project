package clientgithubrepo_test

import (
	"context"
	"testing"

	"github.com/fajarmzky/base-project/pkg/clientgithubrepo"
	"github.com/stretchr/testify/require"
)

func TestDummy(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		c := clientgithubrepo.Dummy{}

		// act
		// assert
		n, err := c.GetPopularRepoNames(context.Background())
		require.NotEmpty(t, n)
		require.NoError(t, err)
	})
}
