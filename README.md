# 👣 Base Project

In what architecture? HAHA (Hopefully Almost Hassle-free Architecture) one. Kidding, the layout is inspired from some popular GitHub repo layouts. If its needed to called a name then maybe a "flat 3-tiers model". This layout is made to be flexible enough to accept DDD/Hexagonal/Event-driven/other abstractions (yes, abstraction, not layout).

Featured parts:

- Flat structured project for easier code traversing
- Designed for modularity and extensibility (easy to copy-paste and modify modules or usecases)
- CLI commands support using clir
- Hot reload support using air via `make watch`
- ENV or YAML configuration option
- Pretty formatted log for development
- Versatile context-based data containers for logging (or communication) via ctxcontent
- Standardized module structure/pattern across components and pkg
- Migration template for SQL databases
- Multiple uniform runtimes with standardized interfaces (httpserver, cronworker, kafkaworker, redisworker for delayed jobs)
- RBAC authorization based on local defition or DB-based definitions
- Centralized hot config binding support using remoteconfig
- Basic modular usecases as services:
  - User service + store (for basic account system)
  - Session service + store (for basic session login, logout, refresh token system)
  - Transaction service (just for sample, no specific functions yet)

## Setting Up

**Dependency and requirements:**

- Go 1.13 or later
- [cosmtrek/air](https://github.com/cosmtrek/air) (to run watch mode)
- [rubenv/sql-migrate](https://github.com/rubenv/sql-migrate) (to run sql migration if you use one)

**How to setup the project:**

1. Clone the repo.
1. Reinit git [like this](https://stackoverflow.com/a/66689268/1914707).
1. Replace all `base-project` string into your desired service name. _If using VSCode you can press Cmd+Shift+F and replace all `base-project` to `nüservice`._ **Notes: please keep `.lamula.yml` intact to keep track of base version.**
1. Read and remove components and pkgs and lines thats not used.
1. Update this README.md as you see fit.
1. Git commit, `day one start making impacts~ 🍃`.

**How to run the application:**

1. Copy config file.

```bash
$ cp config.sample.yml config.yml # adjust values as fit
$ cp .env.sample .env # OPTIONAL
```

2. Run migration.

```bash
$ cp sqlmigrate.sample.yml sqlmigrate.yml # adjust values as fit
$ make migrate
```

3. To run in watch mode: `make watch` (for development purpose)
4. To build in local arch: `make build`

## Philosophies and Rules

- Keep things flat. _Resist the temptation to create subdirectory/subfiles unless we're creating submarines. Usually most of those problems can be solved with good naming._
- If it's hard to understand, it is a valid concern! Start by the assumptions that the code is not good enough.
- Keep things simple and easy. _Don't worry, hardships will come from every direction sooner or later, no need to invite it earlier._
- Try to not make people think when reading a code.
- Fast understandability over fast performance. _Golang is fast enough. And lately clouds are cheap enough to let us spin multiple instances for a service. So yeah, push for common sense but not too far to sacrifice simplicity._
- If it's good, it's copasable. If it's really copasable, then it's perfect.
