package userservice

import (
	"context"
	"time"
)

type Service interface {
	FindUserByUserID(ctx context.Context, in InputFindUserByUserID) (out OutputFindUserByUserID, err error)
	FindUserByLoginUsername(ctx context.Context, in InputFindUserByLoginUsername) (out OutputFindUserByLoginUsername, err error)
	ValidateUserPassword(ctx context.Context, in InputValidateUserPassword) (err error)
	RegisterUser(ctx context.Context, in InputRegisterUser) (out OutputRegisterUser, err error)
}

type InputFindUserByUserID struct {
	UserID string `validate:"required"`
}
type OutputFindUserByUserID struct {
	Result User
}

type InputFindUserByLoginUsername struct {
	LoginUsername string `validate:"required"`
}
type OutputFindUserByLoginUsername struct {
	Result User
}

type InputValidateUserPassword struct {
	User              *User  `validate:"required"`
	AttemptedPassword string `validate:"required"`
}

type InputRegisterUser struct {
	UserID        string `validate:"-"`
	UserType      string `validate:"required"`
	LoginUsername string `validate:"required"`
	LoginPassword string `validate:"required"`
}
type OutputRegisterUser struct {
	Result User
}

// ***

type User struct {
	ID                string                 `validate:"-"` // database id
	UserID            string                 `validate:"required"`
	Type              string                 `validate:"required"`
	LoginUsername     string                 `validate:"required"`
	LoginPasswordHash string                 `validate:"required"`
	Metadata          map[string]interface{} `validate:"required"`
	CreatedAt         time.Time              `validate:"required"`
	UpdatedAt         time.Time              `validate:"required"`
}
