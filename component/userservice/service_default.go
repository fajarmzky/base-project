package userservice

import (
	"context"
	"crypto/sha256"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/component/userstore"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/rs/xid"
)

type Config struct {
	Store userstore.Store `validate:"required"`

	FuncPasswordAddedHasher func(in string) (out string) `validate:"-"`
}

var _ Service = &Default{}

type Default struct {
	Config Config
}

func New(cfg Config) (*Default, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}
	return &Default{Config: cfg}, nil
}

func (d *Default) FindUserByUserID(ctx context.Context, in InputFindUserByUserID) (out OutputFindUserByUserID, err error) {
	// validate input
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// execute
	outFind, err := d.Config.Store.Find(ctx, userstore.InputFind{
		Limit:        1,
		Offset:       0,
		FilterUserID: in.UserID,
	})
	if err != nil {
		err = fmt.Errorf("failure finding data: %w", err)
		return
	}
	if outFind.Total == 0 {
		err = fmt.Errorf("not found")
		return
	}
	usr := outFind.Results[0]

	// build output
	out = OutputFindUserByUserID{
		Result: User(usr),
	}

	return
}

func (d *Default) FindUserByLoginUsername(ctx context.Context, in InputFindUserByLoginUsername) (out OutputFindUserByLoginUsername, err error) {
	// validate input
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// execute
	outFind, err := d.Config.Store.Find(ctx, userstore.InputFind{
		Limit:               1,
		Offset:              0,
		FilterLoginUsername: in.LoginUsername,
	})
	if err != nil {
		err = fmt.Errorf("failure finding data: %w", err)
		return
	}
	if outFind.Total == 0 {
		err = fmt.Errorf("not found")
		return
	}
	usr := outFind.Results[0]

	// build output
	out = OutputFindUserByLoginUsername{
		Result: User(usr),
	}

	return
}

func (d *Default) ValidateUserPassword(ctx context.Context, in InputValidateUserPassword) (err error) {
	// validate input
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build hash
	pwdhash, err := d.HashUserPassword(ctx, InputHashUserPassword{
		UserID:   in.User.UserID,
		Password: in.AttemptedPassword,
	})
	if err != nil {
		err = fmt.Errorf("cant hash password: %w", err)
		return
	}

	// compare
	if in.User.LoginPasswordHash != pwdhash {
		err = fmt.Errorf("mismatching password")
		return
	}

	return
}

func (d *Default) RegisterUser(ctx context.Context, in InputRegisterUser) (out OutputRegisterUser, err error) {
	// validate input
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}
	if in.UserID == "" {
		in.UserID = xid.New().String()
	}

	// find existing user
	outFind, err := d.Config.Store.Find(ctx, userstore.InputFind{
		Limit:               1,
		Offset:              0,
		FilterLoginUsername: in.LoginUsername,
	})
	if err != nil {
		err = fmt.Errorf("failure checking existing user: %w", err)
		return
	}
	if outFind.Total != 0 {
		err = fmt.Errorf("login username already exists")
		return
	}

	// build new user data
	usr := User{
		ID:                "",
		UserID:            in.UserID,
		Type:              in.UserType,
		LoginUsername:     in.LoginUsername,
		LoginPasswordHash: "", // added next
		Metadata:          map[string]interface{}{},
		CreatedAt:         time.Now(),
		UpdatedAt:         time.Now(),
	}

	// build password hash
	pwdhash, err := d.HashUserPassword(ctx, InputHashUserPassword{
		UserID:   usr.UserID,
		Password: in.LoginPassword,
	})
	if err != nil {
		err = fmt.Errorf("cannot hash password: %w", err)
		return
	}

	// assign password hash
	usr.LoginPasswordHash = pwdhash

	// persist new user
	usrStored := userstore.User(usr)
	err = d.Config.Store.Persist(ctx, &usrStored)
	if err != nil {
		err = fmt.Errorf("fail saving new user: %w", err)
		return
	}
	usr = User(usrStored)

	// build output
	out = OutputRegisterUser{
		Result: usr,
	}

	return
}

// ***

type InputHashUserPassword struct {
	UserID   string `validate:"required"`
	Password string `validate:"required"`
}

func (d *Default) HashUserPassword(ctx context.Context, in InputHashUserPassword) (hash string, err error) {
	// validate input
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build base string
	basestr := in.UserID + ":" + in.Password

	// build hash
	h := sha256.New()
	_, err = h.Write([]byte(basestr))
	if err != nil {
		err = fmt.Errorf("failed building pwd bytes: %w", err)
		return
	}
	hash = fmt.Sprintf("%x", h.Sum(nil))

	// custom additional hash layer
	if d.Config.FuncPasswordAddedHasher != nil {
		hash = d.Config.FuncPasswordAddedHasher(hash)
	}

	return
}
