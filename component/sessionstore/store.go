package sessionstore

import (
	"context"
	"errors"
	"time"
)

var (
	ErrNotFound       = errors.New("not found")
	ErrDuplicateEntry = errors.New("duplicate entry")
)

type Store interface {
	Find(ctx context.Context, in InputFind) (out OutputFind, err error)
	Persist(ctx context.Context, in *Session) (err error)
}

type InputFind struct {
	Limit                  int    `validate:"-"`
	Offset                 int    `validate:"-"`
	FilterSessionID        string `validate:"-"`
	FilterAccessTokenHash  string `validate:"-"`
	FilterRefreshTokenHash string `validate:"-"`
}
type OutputFind struct {
	Results []Session
	Total   int
}

// ***

type Session struct {
	ID               string                 `validate:"-"` // database id
	SessionID        string                 `validate:"required"`
	UserID           string                 `validate:"required"`
	SecretString     string                 `validate:"required"`
	Metadata         map[string]interface{} `validate:"required"`
	AccessTokenHash  string                 `validate:"required"`
	AccessExpiredAt  time.Time              `validate:"required"`
	RefreshTokenHash string                 `validate:"required"`
	RefreshExpiredAt time.Time              `validate:"required"`
	CreatedAt        time.Time              `validate:"required"`
	UpdatedAt        time.Time              `validate:"required"`
}
