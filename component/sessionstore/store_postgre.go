package sessionstore

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/lib/pq"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	PostgreTableName = "sessions"
)

type PostgreConfig struct {
	GORM        *gorm.DB `validate:"required,structonly"`
	TablePrefix string   `validate:"-"`
}

type Postgre struct {
	Config PostgreConfig
}

func NewPostgre(conf PostgreConfig) (Store, error) {
	err := validator.Validate(conf)
	if err != nil {
		return nil, err
	}
	return &Postgre{Config: conf}, nil
}

func (p *Postgre) Find(ctx context.Context, in InputFind) (out OutputFind, err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build filter
	type Filter struct {
		SessionID        string
		AccessTokenHash  string
		RefreshTokenHash string
	}
	filter := Filter{
		SessionID:        in.FilterSessionID,
		AccessTokenHash:  in.FilterAccessTokenHash,
		RefreshTokenHash: in.FilterRefreshTokenHash,
	}

	// perform count
	total64 := int64(0)
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Where(filter).
		Count(&total64).Error
	out.Total = int(total64)
	if err != nil {
		err = fmt.Errorf("counting failed: %w", err)
		return
	}
	if out.Total == 0 {
		out.Results = []Session{}
		return
	}

	// perform query
	results := []PostgreSession{}
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Limit(in.Limit).
		Offset(in.Offset).
		Where(filter).
		Find(&results).
		Error
	if err != nil {
		err = fmt.Errorf("query failed: %w", err)
		return
	}

	// build response
	out.Results = []Session{}
	for _, v := range results {
		out.Results = append(out.Results, Session(v))
	}

	return
}

func (p *Postgre) Persist(ctx context.Context, in *Session) (err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// perform query
	data := PostgreSession(*in)
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Clauses(clause.OnConflict{UpdateAll: true}).
		Create(&data).
		Error

	var errPg *pq.Error
	if errors.As(err, &errPg) && errPg.Code == "23505" { // reference: https://www.postgresql.org/docs/9.2/errcodes-appendix.html
		err = ErrDuplicateEntry
	}
	if err != nil {
		err = fmt.Errorf("persistence failure: %w", err)
		return
	}
	in.ID = data.ID

	return err
}

// ***

type PostgreSession struct {
	ID               string                 `validate:"-" gorm:"column:id;default:(-)"`
	SessionID        string                 `validate:"required" gorm:"column:session_id"`
	UserID           string                 `validate:"required" gorm:"column:user_id"`
	SecretString     string                 `validate:"required" gorm:"column:secret_string"`
	Metadata         map[string]interface{} `validate:"required" gorm:"column:metadata"`
	AccessTokenHash  string                 `validate:"required" gorm:"column:access_token_hash"`
	AccessExpiredAt  time.Time              `validate:"required" gorm:"column:access_expired_at"`
	RefreshTokenHash string                 `validate:"required" gorm:"column:refresh_token_hash"`
	RefreshExpiredAt time.Time              `validate:"required" gorm:"column:refresh_expired_at"`
	CreatedAt        time.Time              `validate:"required" gorm:"column:created_at"`
	UpdatedAt        time.Time              `validate:"required" gorm:"column:updated_at"`
}
