package sessionstore

import (
	"context"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	MongoCollectionName = "sessions"
)

var _ Store = &MongoStore{}

type ConfigMongo struct {
	MongoDB *mongo.Database `validate:"required"`
}

type MongoStore struct {
	config ConfigMongo
}

func NewMongoStore(cfg ConfigMongo) (*MongoStore, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	e := &MongoStore{config: cfg}

	return e, nil
}

func (e *MongoStore) Find(ctx context.Context, in InputFind) (out OutputFind, err error) {
	type selector struct {
		SessionID        string `bson:"session_id,omitempty"`
		AccessTokenHash  string `bson:"access_token_hash,omitempty"`
		RefreshTokenHash string `bson:"refresh_token_hash,omitempty"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build data
	opts := options.Find().SetLimit(int64(in.Limit)).SetSkip(int64(in.Offset))
	sel := selector{
		SessionID:        in.FilterSessionID,
		AccessTokenHash:  in.FilterAccessTokenHash,
		RefreshTokenHash: in.FilterRefreshTokenHash,
	}
	// if in.QueryName != "" {
	// 	sel.QueryName = primitive.Regex{Pattern: fmt.Sprintf(".*%s.*", in.QueryName)}
	// }

	// perform count
	count, err := e.config.MongoDB.Collection(MongoCollectionName).CountDocuments(ctx, sel)
	out.Total = int(count)
	if out.Total == 0 {
		out.Results = []Session{}
		return
	}

	// perform query
	results := []MongoSession{}
	cursor, err := e.config.MongoDB.Collection(MongoCollectionName).Find(ctx, sel, opts)
	if err != nil {
		err = fmt.Errorf("query failure: %w", err)
		return
	}
	for cursor.Next(ctx) {
		r := MongoSession{}
		if cursor.Decode(&r); err != nil {
			err = fmt.Errorf("failure decoding mongo cursor: %w", err)
			return
		}
		results = append(results, r)
	}

	// build results
	out.Results = []Session{}
	for _, v := range results {
		out.Results = append(out.Results, Session(v))
	}

	return
}

func (e *MongoStore) Persist(ctx context.Context, in *Session) (err error) {
	type selector struct {
		SessionID string `bson:"session_id"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// prepare data
	sel := selector{SessionID: in.SessionID}
	query := bson.D{{Key: "$set", Value: MongoSession(*in)}}
	opts := options.Update().SetUpsert(true)

	// perform persist
	outUpdate, err := e.config.MongoDB.Collection(MongoCollectionName).UpdateOne(ctx, sel, query, opts)
	if err != nil {
		err = fmt.Errorf("persistence failed: %w", err)
		return
	}
	if in.ID == "" {
		in.ID, _ = outUpdate.UpsertedID.(string)
	}

	return
}

// ***

type MongoSession struct {
	ID               string                 `bson:"_id,omitempty"`
	SessionID        string                 `bson:"session_id", validate:"required"`
	UserID           string                 `bson:"user_id", validate:"required"`
	SecretString     string                 `bson:"secret_string", validate:"required"`
	Metadata         map[string]interface{} `bson:"metadata", validate:"required"`
	AccessTokenHash  string                 `bson:"access_token_hash", validate:"required"`
	AccessExpiredAt  time.Time              `bson:"access_expired_at", validate:"required"`
	RefreshTokenHash string                 `bson:"refresh_token_hash", validate:"required"`
	RefreshExpiredAt time.Time              `bson:"refresh_expired_at", validate:"required"`
	CreatedAt        time.Time              `bson:"created_at", validate:"required"`
	UpdatedAt        time.Time              `bson:"updated_at", validate:"required"`
}
