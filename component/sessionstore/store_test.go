package sessionstore_test

var (
	// integration test variables
	RunIntegration          = false
	IntegrationMongoConnURI = "mongodb://user:password@localhost:27017"
	IntegrationMongoDBName  = "test"
	IntegrationPostgreDSN   = "host=localhost user=user password=password dbname=lamula sslmode=disable"
)
