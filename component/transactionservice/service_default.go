package transactionservice

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/component/transactionstore"
	"github.com/fajarmzky/base-project/pkg/validator"
)

type Config struct {
	Store Store `validate:"required"`

	DelayedTaskPublisherFunc func(taskname, taskid, payload string, processAt time.Time) (err error) `validate:"required"`
}

type Default struct {
	Config Config
}

func New(cfg Config) (Service, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}
	return &Default{Config: cfg}, nil
}

func (d *Default) Find(ctx context.Context, in FindParams) (out Find_Output, err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// perform process
	outFind, err := d.Config.Store.Find(ctx, transactionstore.FindInput{
		TransactionID: in.TransactionID,
		Status:        in.Status,
		PagingLimit:   in.Limit,
		PagingOffset:  in.Offset,
	})
	if err != nil {
		err = fmt.Errorf("find failed: %w", err)
		return
	}

	// build output
	trxs := []Transaction{}
	for _, v := range outFind.Results {
		trxs = append(trxs, Transaction{}.FromStoreData(v))
	}
	out = Find_Output{
		Total:        outFind.Total,
		Transactions: trxs,
	}

	return
}

func (d *Default) Persist(ctx context.Context, in Transaction) (err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// perform process
	data := in.ToStoreData()
	err = d.Config.Store.Persist(ctx, &data)
	if err != nil {
		err = fmt.Errorf("persist failed: %w", err)
		return
	}

	return
}

func (d *Default) PerformSomeBackgroundTask(ctx context.Context, trx Transaction) (err error) {
	// build task payload
	payload := TaskUpdateTransactionStatusInput{XID: "123", TrxID: "1234", Status: "ok"}
	payloadbts, err := json.Marshal(payload)
	if err != nil {
		err = fmt.Errorf("marshalling failure: %w", err)
		return
	}

	// perform process
	taskname := TaskUpdateTransactionStatusType
	taskid := fmt.Sprintf("bgtask:%s", trx.ID)
	err = d.Config.DelayedTaskPublisherFunc(taskname, taskid, string(payloadbts), time.Now().Add(10*time.Minute))
	if err != nil {
		err = fmt.Errorf("task publishing failed: %w", err)
		return
	}

	return
}
