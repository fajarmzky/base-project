package transactionservice_test

import (
	"testing"
	"time"

	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/component/transactionstore"
	"github.com/stretchr/testify/assert"
)

func TestTransaction_ToFromStoreData(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		storedata := transactionstore.Transaction{
			ID:            "1234",
			TransactionID: "8821712321",
			Amount:        12312312,
			Status:        "success",
			CreatedAt:     time.Now(),
			UpdatedAt:     time.Now(),
		}

		// act
		svcobj := transactionservice.Transaction{}.FromStoreData(storedata)
		strobj := svcobj.ToStoreData()

		// assert
		assert.NotEmpty(t, svcobj)
		assert.NotEmpty(t, strobj)
		assert.Equal(t, strobj.ID, storedata.ID)
	})
}
