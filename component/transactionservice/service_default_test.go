package transactionservice_test

import (
	"testing"
	"time"

	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// arrange
		// act
		inst, err := transactionservice.New(transactionservice.Config{
			Store:                    &StoreMock{},
			DelayedTaskPublisherFunc: func(taskname, taskid, payload string, processAt time.Time) (err error) { return },
		})

		// assert
		assert.NoError(t, err)
		assert.NotEmpty(t, inst)
	})
}
