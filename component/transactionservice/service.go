package transactionservice

import (
	"context"
	"time"

	"github.com/fajarmzky/base-project/component/transactionstore"
	"github.com/fajarmzky/base-project/pkg/utils"
)

//go:generate moq -stub -out mocks_test.go -pkg transactionservice_test . Store

type Store transactionstore.Store

// ***

type Service interface {
	Find(ctx context.Context, in FindParams) (out Find_Output, err error)
	Persist(ctx context.Context, trx Transaction) (err error)
	PerformSomeBackgroundTask(ctx context.Context, trx Transaction) (err error)
}

type FindParams struct {
	TransactionID string `validate:"-"`
	Status        string `validate:"-"`
	Limit         int    `validate:"required"`
	Offset        int    `validate:"-"`
}

type Find_Output struct {
	Total        int64
	Transactions []Transaction
}

// ***

type Transaction struct {
	ID            string    `validate:"-"`
	TransactionID string    `validate:"required"`
	Amount        int       `validate:"required"`
	Status        string    `validate:"required"`
	CreatedAt     time.Time `validate:"required"`
	UpdatedAt     time.Time `validate:"required"`
}

func (Transaction) FromStoreData(in transactionstore.Transaction) (out Transaction) {
	utils.CastWithOpts(in, &out, utils.CastOpts{StrictType: true, PanicOnFail: true})
	return
}

func (in Transaction) ToStoreData() (out transactionstore.Transaction) {
	utils.CastWithOpts(in, &out, utils.CastOpts{StrictType: true, PanicOnFail: true})
	return
}

// ***
// tasks struct and datatypes

var (
	TaskUpdateTransactionStatusType = "UPDATE_TRANSACTION_STATUS"
)

type TaskUpdateTransactionStatusInput struct {
	XID    string `validate:"required"`
	TrxID  string `validate:"required"`
	Status string `validate:"required"`
}
