package userstore

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/lib/pq"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	PostgreTableName = "users"
)

type PostgreConfig struct {
	GORM        *gorm.DB `validate:"required,structonly"`
	TablePrefix string   `validate:"-"`
}

type Postgre struct {
	Config PostgreConfig
}

func NewPostgre(conf PostgreConfig) (Store, error) {
	err := validator.Validate(conf)
	if err != nil {
		return nil, err
	}
	return &Postgre{Config: conf}, nil
}

func (p *Postgre) Find(ctx context.Context, in InputFind) (out OutputFind, err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build filter
	type Filter struct {
		UserID        string
		LoginUsername string
	}
	filter := Filter{
		UserID:        in.FilterUserID,
		LoginUsername: in.FilterLoginUsername,
	}

	// perform count
	total64 := int64(0)
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Where(filter).
		Count(&total64).Error
	out.Total = int(total64)
	if err != nil {
		err = fmt.Errorf("counting failed: %w", err)
		return
	}
	if out.Total == 0 {
		out.Results = []User{}
		return
	}

	// perform query
	results := []PostgreUser{}
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Limit(in.Limit).
		Offset(in.Offset).
		Where(filter).
		Find(&results).
		Error
	if err != nil {
		err = fmt.Errorf("query failed: %w", err)
		return
	}

	// build response
	out.Results = []User{}
	for _, v := range results {
		out.Results = append(out.Results, User(v))
	}

	return
}

func (p *Postgre) Persist(ctx context.Context, in *User) (err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// perform query
	data := PostgreUser(*in)
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Clauses(clause.OnConflict{UpdateAll: true}).
		Create(&data).
		Error

	var errPg *pq.Error
	if errors.As(err, &errPg) && errPg.Code == "23505" { // reference: https://www.postgresql.org/docs/9.2/errcodes-appendix.html
		err = ErrDuplicateEntry
	}
	if err != nil {
		err = fmt.Errorf("persistence failure: %w", err)
		return
	}
	in.ID = data.ID

	return err
}

// ***

type PostgreUser struct {
	ID                string                 `validate:"-" gorm:"column:id;default:(-)"`
	UserID            string                 `validate:"required" gorm:"column:user_id"`
	Type              string                 `validate:"required" gorm:"column:type"`
	LoginUsername     string                 `validate:"required" gorm:"column:login_username"`
	LoginPasswordHash string                 `validate:"required" gorm:"column:login_password_hash"`
	Metadata          map[string]interface{} `validate:"required" gorm:"column:metadata"`
	CreatedAt         time.Time              `validate:"required" gorm:"column:created_at"`
	UpdatedAt         time.Time              `validate:"required" gorm:"column:updated_at"`
}
