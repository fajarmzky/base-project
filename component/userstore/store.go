package userstore

import (
	"context"
	"errors"
	"time"
)

var (
	ErrNotFound       = errors.New("not found")
	ErrDuplicateEntry = errors.New("duplicate entry")
)

type Store interface {
	Find(ctx context.Context, in InputFind) (out OutputFind, err error)
	Persist(ctx context.Context, in *User) (err error)
}

type InputFind struct {
	Limit               int    `validate:"-"`
	Offset              int    `validate:"-"`
	FilterUserID        string `validate:"-"`
	FilterLoginUsername string `validate:"-"`
}
type OutputFind struct {
	Results []User
	Total   int
}

// ***

type User struct {
	ID                string                 `validate:"-"` // database id
	UserID            string                 `validate:"required"`
	Type              string                 `validate:"required"`
	LoginUsername     string                 `validate:"required"`
	LoginPasswordHash string                 `validate:"required"`
	Metadata          map[string]interface{} `validate:"required"`
	CreatedAt         time.Time              `validate:"required"`
	UpdatedAt         time.Time              `validate:"required"`
}
