package userstore_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	pkgstore "github.com/fajarmzky/base-project/component/userstore"
)

func TestMongoIntegration(t *testing.T) {
	if !RunIntegration {
		t.SkipNow()
	}

	ctx := context.Background()
	opts := options.Client().ApplyURI(IntegrationMongoConnURI)
	mongoconn, err := mongo.Connect(context.Background(), opts)
	require.NoError(t, err)

	mongodb := mongoconn.Database(IntegrationMongoDBName)

	store, err := pkgstore.NewMongoStore(pkgstore.ConfigMongo{
		MongoDB: mongodb,
	})
	require.NoError(t, err)

	t.Run("ok persist", func(t *testing.T) {
		// arrange
		// act
		err1 := store.Persist(ctx, &pkgstore.User{
			ID:        "adbbc83b-ae99-41b8-ba6a-062a76db9cea",
			UserID:    "adbbc83b",
			Type:      "success",
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		})
		err2 := store.Persist(ctx, &pkgstore.User{
			ID:        "adbbc83c-ae99-41b8-ba6a-062a76db9cea",
			UserID:    "adbbc83c",
			Type:      "success",
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		})

		// assert
		assert.NoError(t, err1)
		assert.NoError(t, err2)
	})

	t.Run("ok find", func(t *testing.T) {
		// arrange
		// act
		out, err := store.Find(ctx, pkgstore.InputFind{
			FilterUserID: "adbbc83b",
			Limit:        10,
			Offset:       0,
		})

		// assert
		assert.NoError(t, err)
		assert.NotEmpty(t, out)
		assert.Equal(t, 1, len(out.Results))
	})

	t.Run("ok find many", func(t *testing.T) {
		// arrange
		// act
		out, err := store.Find(ctx, pkgstore.InputFind{
			Limit:  10,
			Offset: 0,
		})

		// assert
		assert.NoError(t, err)
		assert.NotEmpty(t, out)
		assert.NotEqual(t, 1, len(out.Results))
		assert.NotEqual(t, 1, out.Total)
	})
}
