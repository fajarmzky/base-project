package userstore

import (
	"context"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	MongoCollectionName = "users"
)

var _ Store = &MongoStore{}

type ConfigMongo struct {
	MongoDB *mongo.Database `validate:"required"`
}

type MongoStore struct {
	config ConfigMongo
}

func NewMongoStore(cfg ConfigMongo) (*MongoStore, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	e := &MongoStore{config: cfg}

	return e, nil
}

func (e *MongoStore) Find(ctx context.Context, in InputFind) (out OutputFind, err error) {
	type selector struct {
		UserID        string `bson:"user_id,omitempty"`
		LoginUsername string `bson:"login_username,omitempty"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build data
	opts := options.Find().SetLimit(int64(in.Limit)).SetSkip(int64(in.Offset))
	sel := selector{
		UserID:        in.FilterUserID,
		LoginUsername: in.FilterLoginUsername,
	}
	// if in.QueryName != "" {
	// 	sel.QueryName = primitive.Regex{Pattern: fmt.Sprintf(".*%s.*", in.QueryName)}
	// }

	// perform count
	count, err := e.config.MongoDB.Collection(MongoCollectionName).CountDocuments(ctx, sel)
	out.Total = int(count)
	if out.Total == 0 {
		out.Results = []User{}
		return
	}

	// perform query
	results := []MongoUser{}
	cursor, err := e.config.MongoDB.Collection(MongoCollectionName).Find(ctx, sel, opts)
	if err != nil {
		err = fmt.Errorf("query failure: %w", err)
		return
	}
	for cursor.Next(ctx) {
		r := MongoUser{}
		if cursor.Decode(&r); err != nil {
			err = fmt.Errorf("failure decoding mongo cursor: %w", err)
			return
		}
		results = append(results, r)
	}

	// build results
	out.Results = []User{}
	for _, v := range results {
		out.Results = append(out.Results, User(v))
	}

	return
}

func (e *MongoStore) Persist(ctx context.Context, in *User) (err error) {
	type selector struct {
		UserID string `bson:"user_id"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// prepare data
	sel := selector{UserID: in.UserID}
	query := bson.D{{Key: "$set", Value: MongoUser(*in)}}
	opts := options.Update().SetUpsert(true)

	// perform persist
	outUpdate, err := e.config.MongoDB.Collection(MongoCollectionName).UpdateOne(ctx, sel, query, opts)
	if err != nil {
		err = fmt.Errorf("persistence failed: %w", err)
		return
	}
	if in.ID == "" {
		in.ID, _ = outUpdate.UpsertedID.(string)
	}

	return
}

// ***

type MongoUser struct {
	ID                string                 `bson:"_id,omitempty"` // database id
	UserID            string                 `bson:"user_id"`
	Type              string                 `bson:"type"`
	LoginUsername     string                 `bson:"login_username"`
	LoginPasswordHash string                 `bson:"login_password_hash"`
	Metadata          map[string]interface{} `bson:"metadata"`
	CreatedAt         time.Time              `bson:"created_at"`
	UpdatedAt         time.Time              `bson:"updated_at"`
}
