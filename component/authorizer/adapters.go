package authorizer

import (
	"fmt"

	"github.com/fajarmzky/base-project/pkg/validator"
	"gorm.io/gorm"

	gormadapter "github.com/casbin/gorm-adapter/v3"
	jsonadapter "github.com/casbin/json-adapter/v2"
	mongodbadapter "github.com/casbin/mongodb-adapter/v3"
	mongooptions "go.mongodb.org/mongo-driver/mongo/options"
)

type ConfigAdapterLocal struct{}

func NewAdapterLocal(cfg ConfigAdapterLocal) (out CasbinAdapter, err error) {
	// validate
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	// setup adapter
	tmpstore := []byte("[]")
	out = jsonadapter.NewAdapter(&tmpstore)

	return
}

type ConfigAdapterMongo struct {
	MongoClientOptions *mongooptions.ClientOptions `validate:"required"`
	MongoDatabaseName  string                      `validate:"required"`
}

func NewAdapterMongo(cfg ConfigAdapterMongo) (out CasbinAdapter, err error) {
	// validate
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	// setup adapter
	out, err = mongodbadapter.NewAdapterWithClientOption(cfg.MongoClientOptions, cfg.MongoDatabaseName)
	if err != nil {
		err = fmt.Errorf("cannot setup adapter: %w", err)
		return
	}

	return
}

type ConfigAdapterGORM struct {
	GORMDB *gorm.DB `validate:"required"`
}

func NewAdapterGORM(cfg ConfigAdapterGORM) (out CasbinAdapter, err error) {
	// validate
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	// setup adapter
	out, err = gormadapter.NewAdapterByDB(cfg.GORMDB)
	if err != nil {
		err = fmt.Errorf("cannot setup adapter: %w", err)
		return
	}

	return
}
