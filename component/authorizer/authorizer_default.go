package authorizer

import (
	"context"
	"fmt"
	"time"

	"github.com/casbin/casbin/util"
	"github.com/casbin/casbin/v2"
	"github.com/fajarmzky/base-project/pkg/validator"
)

type Config struct {
	CasbinAdapter          CasbinAdapter `validate:"required"`
	Policies               []Policy      `validate:"required"`
	EnableResourceGrouping bool          `validate:"-"`
}

var _ Authorizer = &Default{}

type Default struct {
	Config         Config
	CasbinEnforcer *casbin.Enforcer
	JSONState      []byte
}

func New(cfg Config) (*Default, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	d := &Default{Config: cfg, JSONState: []byte("[]")}

	// setup instance
	if err := d.Setup(); err != nil {
		err = fmt.Errorf("failed setting up: %w", err)
		return nil, err
	}

	return d, nil
}

func (d *Default) Setup() (err error) {
	// setup casbin
	enf, err := casbin.NewEnforcer(PolicyModel(), d.Config.CasbinAdapter)
	if err != nil {
		err = fmt.Errorf("cannot setup enforcer: %w", err)
		return
	}
	d.CasbinEnforcer = enf

	// enable some options
	if d.Config.EnableResourceGrouping {
		// enable wildcard for resource group
		enf.AddNamedMatchingFunc("g2", "KeyMatch2", util.KeyMatch2)
	}

	// load casbin policy
	err = enf.LoadPolicy()
	if err != nil {
		err = fmt.Errorf("cannot load initial policies: %w", err)
		return
	}

	// inject policies from policy list
	ctxAddPolicy, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err = d.AddPolicies(ctxAddPolicy, d.Config.Policies); err != nil {
		err = fmt.Errorf("cannot inject policy items: %w", err)
		return
	}

	return
}

func (d *Default) AddPolicies(ctx context.Context, in []Policy) (err error) {
	if err = InjectPolicies(d.CasbinEnforcer, in); err != nil {
		err = fmt.Errorf("cannot inject policy items: %w", err)
		return
	}
	if err = d.CasbinEnforcer.SavePolicy(); err != nil {
		err = fmt.Errorf("cannot save injected policy items: %w", err)
		return
	}
	return
}

func (d *Default) RemovePolicies(ctx context.Context, in []Policy) (err error) {
	if err = PluckPolicies(d.CasbinEnforcer, in); err != nil {
		err = fmt.Errorf("cannot pluck policy items: %w", err)
		return
	}
	if err = d.CasbinEnforcer.SavePolicy(); err != nil {
		err = fmt.Errorf("cannot save policy items: %w", err)
		return
	}
	return
}

func (d *Default) Authorize(ctx context.Context, in InputAuthorize) (err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// enforce rule
	ok, err := d.CasbinEnforcer.Enforce(in.Sub, in.Obj, in.Act)
	if err != nil {
		err = fmt.Errorf("failed: %w", err)
		return
	}
	if !ok {
		err = fmt.Errorf("not authorized")
	}

	return
}
