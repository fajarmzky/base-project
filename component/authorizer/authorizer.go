package authorizer

import (
	"context"
	"fmt"

	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
)

type Authorizer interface {
	Authorize(ctx context.Context, in InputAuthorize) (err error)
	AddPolicies(ctx context.Context, in []Policy) (err error)
	RemovePolicies(ctx context.Context, in []Policy) (err error)
}

type InputAuthorize struct {
	Sub string `validate:"required"`
	Act string `validate:"required"`
	Obj string `validate:"required"`
}

// ***

func PolicyModel() (m model.Model) {
	m = model.NewModel()
	_ = m.LoadModelFromText(`
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[role_definition]
g = _ , _
g2 = _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.sub, p.sub) && g2(r.obj, p.obj) && r.act == p.act
`)
	return
}

// ***

type CasbinAdapter interface{}

// ***

type Policy []interface{}

func PolicyAccess(sub, obj, act string) Policy {
	return []interface{}{"p", sub, obj, act}
}

func PolicyRoleSubject(sub, group string) Policy {
	return []interface{}{"g", sub, group}
}

func PolicyRoleObject(obj, group string) Policy {
	return []interface{}{"g2", obj, group}
}

func InjectPolicies(enf *casbin.Enforcer, policies []Policy) (err error) {
	for _, v := range policies {
		code := v[0]
		switch code {
		case "p":
			if _, err = enf.AddPolicy(v[1:]...); err != nil {
				err = fmt.Errorf("policy item inject failed: p: %v", v)
				return
			}
		case "g":
			if _, err = enf.AddGroupingPolicy(v[1:]...); err != nil {
				err = fmt.Errorf("policy item inject failed: g: %v", v)
				return
			}
		case "g2":
			if _, err = enf.AddNamedGroupingPolicy(code.(string), v[1:]...); err != nil {
				err = fmt.Errorf("policy item inject failed: g2: %v", v)
				return
			}
		default:
			err = fmt.Errorf("unknown policy code: %v", code)
			return
		}
	}
	return
}

func PluckPolicies(enf *casbin.Enforcer, policies []Policy) (err error) {
	for _, v := range policies {
		code := v[0]
		switch code {
		case "p":
			if _, err = enf.RemovePolicy(v[1:]...); err != nil {
				err = fmt.Errorf("policy item pluck failed: p: %v", v)
				return
			}
		case "g":
			if _, err = enf.RemoveGroupingPolicy(v[1:]...); err != nil {
				err = fmt.Errorf("policy item pluck failed: g: %v", v)
				return
			}
		case "g2":
			if _, err = enf.RemoveNamedGroupingPolicy(code.(string), v[1:]...); err != nil {
				err = fmt.Errorf("policy item pluck failed: g2: %v", v)
				return
			}
		default:
			err = fmt.Errorf("unknown policy code: %v", code)
			return
		}
	}
	return
}
