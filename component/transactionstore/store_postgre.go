package transactionstore

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/lib/pq"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	PostgreTableName = "transactions"
)

type PostgreConfig struct {
	GORM        *gorm.DB `validate:"required,structonly"`
	TablePrefix string   `validate:"-"`
}

type Postgre struct {
	Config PostgreConfig
}

func NewPostgre(conf PostgreConfig) (Store, error) {
	err := validator.Validate(conf)
	if err != nil {
		return nil, err
	}
	return &Postgre{Config: conf}, nil
}

func (p *Postgre) Find(ctx context.Context, in FindInput) (out FindOutput, err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build filter
	type Filter struct {
		TransactionID string
		Status        string
	}
	filter := Filter{
		TransactionID: in.TransactionID,
		Status:        in.Status,
	}

	// perform count
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Where(filter).
		Count(&out.Total).Error
	if err != nil {
		err = fmt.Errorf("counting failed: %w", err)
		return
	}
	if out.Total == 0 {
		out.Results = []Transaction{}
		return
	}

	// perform query
	results := []PostgreTransaction{}
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Limit(in.PagingLimit).
		Offset(in.PagingOffset).
		Where(filter).
		Find(&results).
		Error
	if err != nil {
		err = fmt.Errorf("query failed: %w", err)
		return
	}

	// build response
	out.Results = []Transaction{}
	for _, v := range results {
		out.Results = append(out.Results, Transaction(v))
	}

	return
}

func (p *Postgre) Persist(ctx context.Context, in *Transaction) (err error) {
	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// perform query
	data := PostgreTransaction(*in)
	err = p.Config.GORM.
		Table(p.Config.TablePrefix + PostgreTableName).
		WithContext(ctx).
		Clauses(clause.OnConflict{UpdateAll: true}).
		Create(&data).
		Error

	var errPg *pq.Error
	if errors.As(err, &errPg) && errPg.Code == "23505" { // reference: https://www.postgresql.org/docs/9.2/errcodes-appendix.html
		err = ErrDuplicateEntry
	}
	if err != nil {
		err = fmt.Errorf("persistence failure: %w", err)
		return
	}
	in.ID = data.ID

	return err
}

// ***

type PostgreTransaction struct {
	ID            string    `gorm:"column:id;default:(-)"` // database given ids
	TransactionID string    `gorm:"column:transaction_id"` // transaction id
	Amount        int       `gorm:"column:amount"`         // in request it is tx_amount
	Status        string    `gorm:"column:status"`         // for check status
	CreatedAt     time.Time `gorm:"column:created_at"`     // using timezone
	UpdatedAt     time.Time `gorm:"column:updated_at"`     // using timezone
}
