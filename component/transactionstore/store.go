package transactionstore

import (
	"context"
	"errors"
	"time"
)

var (
	ErrNotFound       = errors.New("not found")
	ErrDuplicateEntry = errors.New("duplicate entry")
)

type Store interface {
	Find(ctx context.Context, in FindInput) (out FindOutput, err error)
	Persist(ctx context.Context, in *Transaction) (err error)
}

type FindInput struct {
	TransactionID string `validate:"-"`
	Status        string `validate:"-"`
	PagingLimit   int    `validate:"required"`
	PagingOffset  int    `validate:"-"`
}

type FindOutput struct {
	Results []Transaction
	Total   int64
}

// ***

type Transaction struct {
	ID            string    `validate:"-"`        // database given ids
	TransactionID string    `validate:"required"` // transaction id
	Amount        int       `validate:"required"`
	Status        string    `validate:"required"`
	CreatedAt     time.Time `validate:"required"`
	UpdatedAt     time.Time `validate:"required"`
}
