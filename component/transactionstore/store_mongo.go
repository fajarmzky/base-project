package transactionstore

import (
	"context"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/pkg/validator"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	Mongo_CollectionName = "transactions"
)

type ConfigMongo struct {
	MongoDB *mongo.Database `validate:"required"`
}

type Mongo struct {
	config ConfigMongo
}

func NewMongo(cfg ConfigMongo) (Store, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	e := &Mongo{config: cfg}

	return e, nil
}

func (e *Mongo) Find(ctx context.Context, in FindInput) (out FindOutput, err error) {
	type selector struct {
		TrxID  string `bson:"transaction_id,omitempty"`
		Status string `bson:"status,omitempty"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// build data
	opts := options.Find().SetLimit(int64(in.PagingLimit)).SetSkip(int64(in.PagingOffset))
	sel := selector{TrxID: in.TransactionID, Status: in.Status}
	// if in.QueryName != "" {
	// 	sel.QueryName = primitive.Regex{Pattern: fmt.Sprintf(".*%s.*", in.QueryName)}
	// }

	// perform count
	count, err := e.config.MongoDB.Collection(Mongo_CollectionName).CountDocuments(ctx, sel)
	out.Total = int64(count)
	if out.Total == 0 {
		out.Results = []Transaction{}
		return
	}

	// perform query
	results := []Mongo_Transaction{}
	cursor, err := e.config.MongoDB.Collection(Mongo_CollectionName).Find(ctx, sel, opts)
	if err != nil {
		err = fmt.Errorf("query failure: %w", err)
		return
	}
	for cursor.Next(ctx) {
		r := Mongo_Transaction{}
		if cursor.Decode(&r); err != nil {
			err = fmt.Errorf("failure decoding mongo cursor: %w", err)
			return
		}
		results = append(results, r)
	}

	// build results
	out.Results = []Transaction{}
	for _, v := range results {
		out.Results = append(out.Results, Transaction(v))
	}

	return
}

func (e *Mongo) Persist(ctx context.Context, in *Transaction) (err error) {
	type selector struct {
		TrxID string `bson:"transaction_id"`
	}

	// prep and validate
	if err = validator.Validate(in); err != nil {
		err = fmt.Errorf("bad input: %w", err)
		return
	}

	// prepare data
	sel := selector{TrxID: in.TransactionID}
	query := bson.D{{Key: "$set", Value: Mongo_Transaction(*in)}}
	opts := options.Update().SetUpsert(true)

	// perform persist
	outUpdate, err := e.config.MongoDB.Collection(Mongo_CollectionName).UpdateOne(ctx, sel, query, opts)
	if err != nil {
		err = fmt.Errorf("persistence failed: %w", err)
		return
	}
	if in.ID == "" {
		in.ID, _ = outUpdate.UpsertedID.(string)
	}

	return
}

// ***

type Mongo_Transaction struct {
	ID            string    `bson:"_id,omitempty"`  // database given ids
	TransactionID string    `bson:"transaction_id"` // transaction id
	Amount        int       `bson:"amount"`         // in request it is tx_amount
	Status        string    `bson:"status"`         // for check status
	CreatedAt     time.Time `bson:"created_at"`     // using timezone
	UpdatedAt     time.Time `bson:"updated_at"`     // using timezone
}
