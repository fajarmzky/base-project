package sessionservice

import (
	"context"
	"fmt"
	"time"
)

var (
	ErrUnauthenticRefreshToken = fmt.Errorf("unauthentic refresh token")
)

type Service interface {
	Create(ctx context.Context, in InputCreate) (out OutputCreate, err error)
	Validate(ctx context.Context, in InputValidate) (out OutputValidate, err error)
	Refresh(ctx context.Context, in InputRefresh) (out OutputRefresh, err error)
	Expire(ctx context.Context, in InputExpire) (err error)
}

type InputCreate struct {
	SessionID    string                 `validate:"required"`
	UserID       string                 `validate:"required"`
	SecretString string                 `validate:"required"`
	Metadata     map[string]interface{} `validate:"required"`
}
type OutputCreate struct {
	Session      Session
	AccessToken  string
	RefreshToken string
}

type InputValidate struct {
	AccessToken string `validate:"required"`
}
type OutputValidate struct {
	Session Session
}

type InputRefresh struct {
	RefreshToken string `validate:"required"`
}
type OutputRefresh struct {
	Session      Session
	AccessToken  string
	RefreshToken string
}

type InputExpire struct {
	RefreshToken string `validate:"required"`
}

// ***

type Session struct {
	ID               string                 `validate:"-"` // database id
	SessionID        string                 `validate:"required"`
	UserID           string                 `validate:"required"`
	SecretString     string                 `validate:"required"`
	Metadata         map[string]interface{} `validate:"required"`
	AccessTokenHash  string                 `validate:"required"`
	AccessExpiredAt  time.Time              `validate:"required"`
	RefreshTokenHash string                 `validate:"required"`
	RefreshExpiredAt time.Time              `validate:"required"`
	CreatedAt        time.Time              `validate:"required"`
	UpdatedAt        time.Time              `validate:"required"`
}
