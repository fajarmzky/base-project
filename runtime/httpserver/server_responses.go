package httpserver

type Resp struct {
	Status  RespCode    `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type RespCode string

func (e Resp) WithMessage(msg string) Resp {
	e.Message += ": " + msg
	return e
}

func (e Resp) Normalize() Resp {
	// send empty object instead of nil
	if e.Data == nil {
		e.Data = map[string]interface{}{}
	}

	return e
}

// **

type RespKind int

const (
	Success RespKind = iota
	Pending
	ErrUnauthorized
	ErrPathNotFound
	ErrValidation
	ErrDataNotFound
	ErrProcessFailed
	ErrUnexpected
	ErrRateLimited
)

var RespPresets map[RespKind]Resp = map[RespKind]Resp{
	Success:          {Status: "00", Message: "success"},
	Pending:          {Status: "01", Message: "process pending"},
	ErrUnauthorized:  {Status: "02", Message: "not authorized for resource"},
	ErrPathNotFound:  {Status: "03", Message: "path not found"},
	ErrValidation:    {Status: "04", Message: "validation error"},
	ErrDataNotFound:  {Status: "05", Message: "data not found"},
	ErrProcessFailed: {Status: "06", Message: "process failed"},
	ErrRateLimited:   {Status: "07", Message: "rate limited"},
	ErrUnexpected:    {Status: "99", Message: "unexpected error"},
}
