package httpserver

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/fajarmzky/base-project/component/authorizer"
	"github.com/fajarmzky/base-project/component/sessionservice"
	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/component/userservice"
	"github.com/fajarmzky/base-project/pkg/clientgithubrepo"
	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	ctxkeyUnhandledError = "hidden-error"
)

type Runtime interface {
	Run() error
	Close() error
}

type Config struct {
	ServiceID           string    `validate:"required"`
	InstanceID          string    `validate:"required"`
	InfoVersion         string    `validate:"required"`
	InfoListenedPort    int       `validate:"required"`
	InfoServerStartTime time.Time `validate:"required"`
	InfoLocalIPAddress  string    `validate:"required"`

	RootContext                          context.Context   `validate:"required"`
	EnableVerboseUnexpectedErrorResponse bool              `validate:"-"`
	RateLimiterMaxRequest                int               `validate:"-"`        // Max request per second allowed. Set to 0 to disable request rate limiting.
	RateLimiterRefreshInterval           time.Duration     `validate:"required"` // How long needed to wait until next request allowed
	LocalAuthSecretString                string            `validate:"required"`
	LocalAuthCredMap                     map[string]string `validate:"required"`

	Authorizer         authorizer.Authorizer      `validate:"required"`
	TransactionService transactionservice.Service `validate:"required"`
	UserService        userservice.Service        `validate:"required"`
	SessionService     sessionservice.Service     `validate:"required"`
	ClientGithubRepo   clientgithubrepo.Client    `validate:"required"`
}

var _ Runtime = &Server{}

type Server struct {
	config      Config
	engine      *http.Server
	router      http.Handler
	routerflags map[*regexp.Regexp][]RouterFlag
}

func New(cfg Config) (*Server, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	s := Server{
		config:      cfg,
		router:      echo.New(),
		routerflags: map[*regexp.Regexp][]RouterFlag{},
	}

	// setup routes
	if err := s.setupRoutes(); err != nil {
		return nil, err
	}

	return &s, nil
}

// setupRoutes defines mappings of server paths to handler's functions
func (s *Server) setupRoutes() (err error) {
	// ---
	// setup h
	h := Handlers{Config: s.config}

	// ---
	// setup r
	r := s.router.(*echo.Echo)
	r.Validator = &CustomValidator{}
	r.Binder = &CustomBinder{}

	r.GET("/ping", h.HandlePing())

	r.POST("/sessions/create", h.HandleSessionsCreate())
	r.POST("/sessions/refresh", h.HandleSessionsRefresh())
	r.POST("/sessions/expire", h.HandleSessionsExpire())

	r.POST("/transactions/create", h.HandleTransactionsCreate())
	r.POST("/transactions/list", h.HandleTransactionsList())

	r.POST("/samples/listrepositories", h.HandleSamplesCheckPopularRepo())

	r.POST("/x/generatebasiccreds", h.HandleXGenerateBasicCredential())

	// register custom flags for routes
	s.SetRouterFlags("/ping", RouterFlagSkipAuth, RouterFlagSkipLog)
	s.SetRouterFlags("/sessions/create", RouterFlagSkipAuth, RouterFlagSkipPayloadLogging)
	s.SetRouterFlags("/sessions/refresh", RouterFlagSkipPayloadLogging)

	// middlewares and add-ons
	r.Pre(middleware.RemoveTrailingSlash())
	r.Pre(s.MWRateLimit(ConfigMWRateLimit{
		MaxRequest:    s.config.RateLimiterMaxRequest,
		ClearInterval: s.config.RateLimiterRefreshInterval,
	}))
	r.Pre(echo.WrapMiddleware(s.MWCaseInsensitiveURLMatching()))
	r.Use(s.MWSetupAuthContext())
	r.Use(middleware.RecoverWithConfig(middleware.RecoverConfig{
		DisableStackAll:   true,
		DisablePrintStack: true,
	}))

	r.HTTPErrorHandler = s.handleError

	s.router = r
	return
}

// handleError defines how to handle unhandled labstack echo's errors
func (s *Server) handleError(err error, c echo.Context) {
	ctx := c.Request().Context()
	ctxcontent.SetData(ctx, ctxkeyUnhandledError, err.Error())

	if c.Response().Committed {
		return
	}

	// set default error
	resp := RespPresets[ErrUnexpected]
	if s.config.EnableVerboseUnexpectedErrorResponse {
		resp.Message += ": " + err.Error() // write complete error message in response
	}

	// catch http errors
	if httpError, ok := err.(*echo.HTTPError); ok {
		switch httpError.Code {
		case http.StatusNotFound:
			resp = RespPresets[ErrPathNotFound]
		case http.StatusUnauthorized:
			resp = RespPresets[ErrUnauthorized]
		default:
			resp.Message = fmt.Sprintf("%s: %s (%d)", resp.Message, httpError.Message, httpError.Code)
		}
	}

	// send response
	err = c.JSON(http.StatusOK, resp.Normalize())
	if err != nil {
		fmt.Println(err)
	}
}

func (s *Server) Run() error {
	s.engine = &http.Server{
		Addr:    fmt.Sprintf(":%d", s.config.InfoListenedPort),
		Handler: s.router,
	}
	if err := s.engine.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		err = fmt.Errorf("http server listen error: %w", err)
		return err
	}
	return nil
}

func (s *Server) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	if err := s.engine.Shutdown(ctx); err != nil {
		err = fmt.Errorf("engine shutdown error: %w", err)
		return err
	}

	return nil
}

// ***

type CustomValidator struct{}

func (cv *CustomValidator) Validate(i interface{}) error {
	return validator.ValidateWithOpts(i, validator.Opts{Mode: validator.ModeCompact})
}

type CustomBinder struct{}

// Bind will bind request body as JSON with strict payload.
// Otherwise fallback with echo's internal.
func (b *CustomBinder) Bind(i interface{}, c echo.Context) (err error) {
	req := c.Request()
	if req.ContentLength == 0 {
		return
	}

	cType := req.Header.Get(echo.HeaderContentType)
	if !strings.HasPrefix(cType, echo.MIMEApplicationJSON) {
		ori := echo.DefaultBinder{}
		return ori.Bind(i, c)
	}

	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()
	err = dec.Decode(i)
	if err != nil {
		if ute, ok := err.(*json.UnmarshalTypeError); ok {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("Unmarshal type error: expected=%v, got=%v, "+
					"field=%v, offset=%v", ute.Type, ute.Value, ute.Field, ute.Offset)).
				SetInternal(err)
		} else if se, ok := err.(*json.SyntaxError); ok {
			return echo.NewHTTPError(http.StatusBadRequest,
				fmt.Sprintf("Syntax error: offset=%v, error=%v", se.Offset, se.Error())).
				SetInternal(err)
		}
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	return nil
}
