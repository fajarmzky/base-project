package httpserver

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"time"

	"github.com/fajarmzky/base-project/component/authorizer"
	"github.com/fajarmzky/base-project/component/sessionservice"
	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/fajarmzky/base-project/pkg/ctxlocal"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/xid"
	"golang.org/x/time/rate"
)

func (s *Server) MWSetupAuthContext() echo.MiddlewareFunc {
	authorizeSessionToken := func(ctx context.Context, headers http.Header) (err error) {
		// skip if already set
		authData := ctxlocal.GetAuthData(ctx)
		if authData.SubjectID != "" {
			return
		}

		// get bearer token value
		tmp := strings.Split(headers.Get("Authorization"), "Bearer ")
		if len(tmp) < 2 {
			err = fmt.Errorf("bad bearer token")
			return
		}
		token := tmp[1]

		// validate bearer token
		outValidate, err := s.config.SessionService.Validate(ctx, sessionservice.InputValidate{
			AccessToken: token,
		})
		if err != nil {
			err = fmt.Errorf("failed validating token: %w", err)
			return
		}

		// set ctx auth
		ctxlocal.SetAuthData(ctx, ctxlocal.AuthData{
			SubjectID: outValidate.Session.UserID,
		})

		return
	}

	authorizeBasicAuth := func(ctx context.Context, headers http.Header) (err error) {
		// skip if already set
		authData := ctxlocal.GetAuthData(ctx)
		if authData.SubjectID != "" {
			return
		}

		// get basic auth value
		tmp := strings.Split(headers.Get("Authorization"), "Basic ")
		if len(tmp) < 2 {
			err = fmt.Errorf("bad basic token")
			return
		}
		credRawB64 := tmp[1]

		// decode cred from b64
		btsrawcred, err := base64.StdEncoding.DecodeString(credRawB64)
		if err != nil {
			err = fmt.Errorf("undecodeable characters: %w", err)
			return err
		}

		// validate bearer token
		outSplitCred := strings.SplitN(string(btsrawcred), ":", 2)
		if len(outSplitCred) < 2 {
			err = fmt.Errorf("bad token cred")
			return
		}
		credUsername := outSplitCred[0]
		credPassword := outSplitCred[1]

		// - hash password
		pwdHashRaw := fmt.Sprintf("%s:%s:%s", s.config.LocalAuthSecretString, credUsername, credPassword)
		h := sha1.New()
		_, _ = h.Write([]byte(pwdHashRaw))
		pwdHash := fmt.Sprintf("%x", h.Sum(nil))

		// - match password
		if _, found := s.config.LocalAuthCredMap[credUsername]; !found {
			err = fmt.Errorf("unknown user")
			return
		}
		if s.config.LocalAuthCredMap[credUsername] != pwdHash {
			err = fmt.Errorf("bad password")
			return
		}

		// set ctx auth
		ctxlocal.SetAuthData(ctx, ctxlocal.AuthData{
			SubjectID: credUsername,
		})

		return
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			req := c.Request()
			ctx := req.Context()

			// try session token auth
			if strings.HasPrefix(req.Header.Get("Authorization"), "Bearer") {
				if err = authorizeSessionToken(ctx, req.Header); err != nil {
					err = fmt.Errorf("session auth failed: %w", err)
					ctxcontent.AddWarning(ctx, err.Error()) // always keep error
					return err
				}
			}

			// try basic auth
			if strings.HasPrefix(req.Header.Get("Authorization"), "Basic") {
				if err = authorizeBasicAuth(ctx, req.Header); err != nil {
					err = fmt.Errorf("basic auth failed: %w", err)
					ctxcontent.AddWarning(ctx, err.Error()) // always keep error
					return err
				}
			}

			// ... try other auth models. e.g. usertoken, access token, basic, etc.

			// check if validation skippable
			if s.CheckRouterFlag(req.URL.String(), RouterFlagSkipAuth) {
				return next(c)
			}

			// validate authorized
			if authData := ctxlocal.GetAuthData(ctx); authData.SubjectID == "" {
				err = fmt.Errorf("cannot infer valid auth user")
				return err
			}

			return next(c)
		}
	}
}

func (s *Server) MWCaseInsensitiveURLMatching() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			req.URL.Path = strings.ToLower(req.URL.Path)
			next.ServeHTTP(w, req)
		})
	}
}

type ConfigMWRateLimit struct {
	MaxRequest    int
	ClearInterval time.Duration
}

func (s *Server) MWRateLimit(cfg ConfigMWRateLimit) echo.MiddlewareFunc {
	RPSLimit := cfg.MaxRequest
	RPSClearInterval := cfg.ClearInterval

	return middleware.RateLimiterWithConfig(middleware.RateLimiterConfig{
		Skipper: func(c echo.Context) bool {
			req := c.Request()

			// skip if max request is 0 = means disabled
			if cfg.MaxRequest == 0 {
				return true
			}
			// skip if url exist in skip list
			if s.CheckRouterFlag(req.URL.String(), RouterFlagSkipGenericRateLimit) {
				return true
			}
			return false
		},
		Store: middleware.NewRateLimiterMemoryStoreWithConfig(
			middleware.RateLimiterMemoryStoreConfig{Rate: rate.Limit(RPSLimit), ExpiresIn: RPSClearInterval},
		),
		IdentifierExtractor: func(c echo.Context) (string, error) {
			id := c.RealIP()
			return id, nil
		},
		ErrorHandler: func(c echo.Context, err error) error {
			resp := RespPresets[ErrUnauthorized]
			resp.Message += ": cannot identify user"
			return c.JSON(http.StatusOK, resp)
		},
		DenyHandler: func(c echo.Context, identifier string, err error) error {
			resp := RespPresets[ErrRateLimited]
			resp.Message += ": too many request"
			return c.JSON(http.StatusOK, resp)
		},
	})
}

type ConfigMWStalk struct {
	CorrelationIDHeaderKey string
	JourneyIDHeaderKey     string
}

// MWStalk is a final middleware to stalk the request and response
func (s *Server) MWStalk(config ConfigMWStalk) func(_ http.Handler) http.Handler {
	// readRequestIP := func(r *http.Request) string {
	// 	IPAddress := r.Header.Get("X-Real-Ip")
	// 	if IPAddress == "" {
	// 		IPAddress = r.Header.Get("X-Forwarded-For")
	// 	}
	// 	if IPAddress == "" {
	// 		IPAddress = r.RemoteAddr
	// 	}
	// 	return IPAddress
	// }

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			// init and prep
			ctx := req.Context()
			// timestamp := time.Now()
			// svcinfo := ctxlocal.GetServiceInfo(s.config.RootContext)

			// acquire ids
			cid := strings.TrimSpace(req.Header.Get(config.CorrelationIDHeaderKey))
			if cid == "" {
				cid = xid.New().String()
			}

			jid := strings.TrimSpace(req.Header.Get(config.JourneyIDHeaderKey))
			if jid == "" {
				jid = xid.New().String()
			}

			// stalk context
			ctx = ctxcontent.CreateWith(ctx)
			// ctx = stdlogger.InjectCtx(ctx, stdlogger.Context{
			// 	ServiceName:    svcinfo.ServiceID,
			// 	ServiceVersion: svcinfo.InfoVersion,
			// 	ServicePort:    svcinfo.InfoListenedPort,
			// 	ThreadID:       cid, // thread id is always new per request
			// 	JourneyID:      jid, // journey id is from user's app
			// 	ChainID:        cid, // correlation id is from front service, usually gateway
			// 	Tag:            "instance_" + svcinfo.InstanceID,
			// 	ReqMethod:      req.Method,
			// 	ReqURI:         req.URL.RequestURI(),
			// })

			// setup authorizer
			ctxcontent.SetAuthorizer(ctx, &AuthorizerWrapper{Authorizer: s.config.Authorizer})

			// sniff request body
			var reqBytes []byte
			if req.Body != nil {
				tmp, err := ioutil.ReadAll(req.Body)
				if err != nil {
					ctxcontent.AddWarning(ctx, err.Error()) // always keep error
					tmp = []byte{}                          // set empty bytes
				}
				reqBytes = tmp
				req.Body = ioutil.NopCloser(bytes.NewBuffer(reqBytes))
			}

			// perform original request handling with recorder stub
			rec := httptest.NewRecorder()
			next.ServeHTTP(rec, req.WithContext(ctx))
			if req.MultipartForm != nil {
				_ = req.MultipartForm.RemoveAll()
			}

			// sniff response
			respBytes, err := ioutil.ReadAll(rec.Body)
			if err != nil {
				ctxcontent.AddWarning(ctx, err.Error()) // always keep error
				respBytes = []byte{}                    // set empty bytes
			}

			for k, v := range rec.Result().Header {
				w.Header().Set(k, strings.Join(v, "; "))
			}

			// * write back response body
			w.Header().Set(config.CorrelationIDHeaderKey, cid)
			w.Header().Set(config.JourneyIDHeaderKey, jid)
			w.WriteHeader(rec.Code)
			if _, err = bytes.NewReader(respBytes).WriteTo(w); err != nil {
				ctxcontent.AddWarning(ctx, err.Error()) // always keep error
			}

			// check whether to skip all logging
			// shouldSkipLog := false
			// if s.CheckRouterFlag(req.URL.RawPath, RouterFlagSkipLog) {
			// 	shouldSkipLog = true
			// }

			// check whether to skip payload logging
			// shouldSkipPayloadLogging := false
			// if s.CheckRouterFlag(req.URL.RawPath, RouterFlagSkipPayloadLogging) {
			// 	shouldSkipPayloadLogging = true
			// }

			// build tdr log
			// if !shouldSkipLog {
			// type TDRLog logger.TDR

			// // * try request body as json, string if not empty, or fallback to empty object
			// var reqBody interface{}
			// if err := json.Unmarshal(reqBytes, &reqBody); err != nil && len(reqBytes) != 0 {
			// 	reqBody = string(reqBytes)
			// }
			// if reqBody == nil {
			// 	reqBody = map[string]interface{}{}
			// }
			// if shouldSkipPayloadLogging {
			// 	reqBody = map[string]interface{}{"payload_logging_disabled": true}
			// }

			// // * try response body as json, string if not empty, or fallback to empty object
			// var respBody interface{}
			// if err := json.Unmarshal(respBytes, &respBody); err != nil && len(respBytes) != 0 {
			// 	respBody = string(respBytes)
			// }
			// if respBody == nil {
			// 	respBody = map[string]interface{}{}
			// }
			// if shouldSkipPayloadLogging {
			// 	respBody = map[string]interface{}{"payload_logging_disabled": true}
			// }

			// // * try getting final error from error handler
			// errmsg, ok := ctxcontent.GetData(ctx, ctxkeyUnhandledError).(string)
			// if !ok {
			// 	errmsg = ""
			// }

			// tdrData := TDRLog{
			// 	AppName:    svcinfo.ServiceID,
			// 	AppVersion: svcinfo.InfoVersion,
			// 	ThreadID:   cid,
			// 	JourneyID:  jid,
			// 	ChainID:    cid,

			// 	Path:         req.URL.RequestURI(),
			// 	Method:       req.Method,
			// 	IP:           svcinfo.InfoLocalIPAddress,
			// 	Port:         svcinfo.InfoListenedPort,
			// 	SrcIP:        readRequestIP(req),
			// 	RespTime:     time.Since(timestamp).Milliseconds(),
			// 	ResponseCode: rec.Result().Status,

			// 	Header:  utils.FmtCompactHTTPHeader(req.Header),
			// 	Request: reqBody,
			// 	Response: logger.TDRResp{
			// 		Status:  fmt.Sprint(rec.Result().Status),
			// 		Message: "",
			// 		Header:  utils.FmtCompactHTTPHeader(w.Header()),
			// 		Data:    respBody,
			// 	},
			// 	Error: errmsg,

			// 	AdditionalData: map[string]interface{}{
			// 		"path":     req.URL.Path, // needed to keep 'clean url' for traffic grouping
			// 		"vars":     ctxcontent.ListVars(ctx),
			// 		"warnings": ctxcontent.ListWarnings(ctx),
			// 		"process":  ctxcontent.ListProcesses(ctx),
			// 	},
			// }

			// logger.Instance().Debug(ctx, "tdr transaction performed", logger.RemarshalToKVs(tdrData)...)
			// logger.Instance().TDR(ctx, stdlogger.LogTdrModel(tdrData))
			// }
		})
	}
}

// ***

type AuthorizerWrapper struct{ authorizer.Authorizer }

func (w *AuthorizerWrapper) Authorize(ctx context.Context, action string, resourceID string) (err error) {
	// acquire subjectID
	authData := ctxlocal.GetAuthData(ctx)
	if authData.SubjectID == "" {
		err = fmt.Errorf("auth subject undefined")
		return
	}

	// iterate actions
	if err = w.Authorizer.Authorize(ctx, authorizer.InputAuthorize{
		Sub: authData.SubjectID,
		Act: action,
		Obj: resourceID,
	}); err != nil {
		err = fmt.Errorf("authorization failed: %w", err)
		return
	}

	return
}

// ***

type RouterFlag int

const (
	RouterFlagSkipAuth RouterFlag = iota + 1
	RouterFlagSkipGenericRateLimit
	RouterFlagSkipLog
	RouterFlagSkipPayloadLogging
)

func (s *Server) SetRouterFlags(matcher string, flags ...RouterFlag) {
	r := regexp.MustCompile("^" + matcher) // add start of string matcher
	if s.routerflags[r] == nil {
		s.routerflags[r] = []RouterFlag{}
	}
	s.routerflags[r] = append(s.routerflags[r], flags...)
}

func (s *Server) CheckRouterFlag(path string, flag RouterFlag) (exist bool) {
	for matcher, flags := range s.routerflags {
		if matcher.MatchString(path) {
			for _, fl := range flags {
				if fl == flag {
					return true
				}
			}
			return // always exit upon match
		}
	}
	return
}
