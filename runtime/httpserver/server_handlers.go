package httpserver

import (
	"context"
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"time"

	"github.com/fajarmzky/base-project/component/sessionservice"
	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/component/userservice"
	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/gorilla/schema"
	"github.com/labstack/echo/v4"
	"github.com/rs/xid"
)

type Handlers struct{ Config Config }

func (h *Handlers) HandlePing() echo.HandlerFunc {
	type RequestData struct {
	}

	type ResponseData struct {
		ID             string    `json:"id"`
		State          string    `json:"state"`
		StartTimestamp time.Time `json:"start_time"`
		Uptime         string    `json:"uptime"`
	}

	return func(c echo.Context) error {
		// var ctx = c.Request().Context()

		// process
		var response Resp = RespPresets[Success]
		response.Data = ResponseData{
			ID:             h.Config.ServiceID + "@" + h.Config.InstanceID,
			State:          "healthy",
			StartTimestamp: h.Config.InfoServerStartTime,
			Uptime:         time.Since(h.Config.InfoServerStartTime).Round(time.Second).String(),
		}

		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleXGenerateBasicCredential() echo.HandlerFunc {
	const AuthResourceName = "resgroup.superadmin"

	type RequestData struct {
		BasicUsername    string `json:"basic_username" validate:"required"`
		BasicPasswordB64 string `json:"basic_password_b64" validate:"required"`
	}

	type ResponseData struct {
		BasicUsername     string `json:"basic_username"`
		BasicPasswordHash string `json:"basic_password_hash"`
	}

	return func(c echo.Context) (err error) {
		var ctx = c.Request().Context()

		// prep and validate
		reqdata := RequestData{}
		if err := ctxcontent.Authorize(ctx, "use", AuthResourceName); err != nil {
			ctxcontent.AddWarning(ctx, err.Error()) // always keep error
			return c.JSON(200, RespPresets[ErrUnauthorized].WithMessage("endpoint not allowed").Normalize())
		}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// process
		// - decode password
		btsrawpwd, err := base64.StdEncoding.DecodeString(reqdata.BasicPasswordB64)
		if err != nil {
			err = fmt.Errorf("undecodeable characters: %w", err)
			return err
		}

		// - hash as password hash
		pwdHashRaw := fmt.Sprintf("%s:%s:%s", h.Config.LocalAuthSecretString, reqdata.BasicUsername, string(btsrawpwd))
		h := sha1.New()
		_, _ = h.Write([]byte(pwdHashRaw))
		pwdHash := fmt.Sprintf("%x", h.Sum(nil))

		// build output
		var response Resp = RespPresets[Success]
		response.Data = ResponseData{
			BasicUsername:     reqdata.BasicUsername,
			BasicPasswordHash: pwdHash,
		}

		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleSessionsCreate() echo.HandlerFunc {
	type RequestData struct {
		Username    string `json:"username" validate:"required"`
		PasswordB64 string `json:"password_b64" validate:"required"`
	}

	type ResponseData struct {
		AccessToken  string    `json:"access_token"`
		AccessExpAt  time.Time `json:"access_exp_at"`
		RefreshToken string    `json:"refresh_token"`
		RefreshExpAt time.Time `json:"refresh_exp_at"`
	}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		reqdata := RequestData{}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// perform process
		// - decode password
		btsrawpwd, err := base64.StdEncoding.DecodeString(reqdata.PasswordB64)
		if err != nil {
			err = fmt.Errorf("undecodeable characters: %w", err)
			return err
		}

		// - find user by username
		outFindUser, err := h.Config.UserService.FindUserByLoginUsername(ctx, userservice.InputFindUserByLoginUsername{
			LoginUsername: reqdata.Username,
		})
		if err != nil {
			err = fmt.Errorf("cannot find user: %w", err)
			return err
		}

		// - validate password
		err = h.Config.UserService.ValidateUserPassword(ctx, userservice.InputValidateUserPassword{
			User:              &outFindUser.Result,
			AttemptedPassword: string(btsrawpwd),
		})
		if err != nil {
			err = fmt.Errorf("wrong password: %w", err)
			return err
		}

		// - check existing sessions
		// TODO: add this check to disable multiple logins

		// - create session
		sessionid := xid.New().String()
		secretstring := xid.New().String() + xid.New().String()
		outCreateSession, err := h.Config.SessionService.Create(ctx, sessionservice.InputCreate{
			SessionID:    sessionid,
			UserID:       outFindUser.Result.UserID,
			SecretString: secretstring,
			Metadata:     map[string]interface{}{
				// add session data if needed
			},
		})
		if err != nil {
			err = fmt.Errorf("failed creating session: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{
			AccessToken:  outCreateSession.AccessToken,
			AccessExpAt:  outCreateSession.Session.AccessExpiredAt,
			RefreshToken: outCreateSession.RefreshToken,
			RefreshExpAt: outCreateSession.Session.RefreshExpiredAt,
		}
		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleSessionsRefresh() echo.HandlerFunc {
	type RequestData struct {
		RefreshToken string `json:"refresh_token" validate:"required"`
	}

	type ResponseData struct {
		AccessToken  string    `json:"access_token"`
		AccessExpAt  time.Time `json:"access_exp_at"`
		RefreshToken string    `json:"refresh_token"`
		RefreshExpAt time.Time `json:"refresh_exp_at"`
	}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		reqdata := RequestData{}
		if err := ctxcontent.Authorize(ctx, "use", "res.sessions.refresh"); err != nil {
			ctxcontent.AddWarning(ctx, err.Error()) // always keep error
			return c.JSON(200, RespPresets[ErrUnauthorized].WithMessage("endpoint not allowed").Normalize())
		}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// perform process
		// - refresh session
		outRefreshSession, err := h.Config.SessionService.Refresh(ctx, sessionservice.InputRefresh{
			RefreshToken: reqdata.RefreshToken,
		})
		if err != nil {
			err = fmt.Errorf("failed refreshing session: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{
			AccessToken:  outRefreshSession.AccessToken,
			AccessExpAt:  outRefreshSession.Session.AccessExpiredAt,
			RefreshToken: outRefreshSession.RefreshToken,
			RefreshExpAt: outRefreshSession.Session.RefreshExpiredAt,
		}
		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleSessionsExpire() echo.HandlerFunc {
	type RequestData struct {
		RefreshToken string `json:"refresh_token" validate:"required"`
	}

	type ResponseData struct{}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		reqdata := RequestData{}
		if err := ctxcontent.Authorize(ctx, "use", "res.sessions.expire"); err != nil {
			ctxcontent.AddWarning(ctx, err.Error()) // always keep error
			return c.JSON(200, RespPresets[ErrUnauthorized].WithMessage("endpoint not allowed").Normalize())
		}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// perform process
		// - refresh session
		err := h.Config.SessionService.Expire(ctx, sessionservice.InputExpire{
			RefreshToken: reqdata.RefreshToken,
		})
		if err != nil {
			err = fmt.Errorf("failed expiring session: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{}
		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleTransactionsCreate() echo.HandlerFunc {
	type RequestData struct {
		TrxID  string `json:"trx_id" validate:"required"`
		Amount int    `json:"amount" validate:"required"`
		Status string `json:"status" validate:"required"`
	}

	type ResponseData struct {
		TrxID     string    `json:"trx_id" validate:"required"`
		Amount    int       `json:"amount" validate:"required"`
		Status    string    `json:"status" validate:"required"`
		CreatedAt time.Time `json:"created_at" validate:"required"`
		UpdatedAt time.Time `json:"updated_at" validate:"required"`
	}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		reqdata := RequestData{}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// perform process
		trx := transactionservice.Transaction{
			ID:            "",
			TransactionID: reqdata.TrxID,
			Amount:        reqdata.Amount,
			Status:        reqdata.Status,
			CreatedAt:     time.Now(),
			UpdatedAt:     time.Now(),
		}
		err := h.Config.TransactionService.Persist(ctx, trx)
		if err != nil {
			err = fmt.Errorf("create failed: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{
			TrxID:     trx.TransactionID,
			Amount:    trx.Amount,
			Status:    trx.Status,
			CreatedAt: trx.CreatedAt,
			UpdatedAt: trx.UpdatedAt,
		}
		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleTransactionsList() echo.HandlerFunc {
	type RequestParams struct {
		Limit  int `schema:"limit" validate:"-"`
		Offset int `schema:"offset" validate:"-"`
	}

	type RequestData struct {
	}

	type ResponseData struct {
		Results []transactionservice.Transaction `json:"results"`
		Total   int32                            `json:"total"`
		Limit   int32                            `json:"limit"`
		Offset  int32                            `json:"offset"`
	}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		reqparams := RequestParams{}
		reqdata := RequestData{}

		urldecoder := schema.NewDecoder()
		urldecoder.IgnoreUnknownKeys(true)
		if err := urldecoder.Decode(&reqparams, c.QueryParams()); err != nil {
			err = fmt.Errorf("query params binding failed: %w", err)
			return err
		}
		if err := c.Bind(&reqdata); err != nil {
			err = fmt.Errorf("cannot bind inputs: %w", err)
			return err
		}
		if err := validator.Validate(reqdata); err != nil {
			err = fmt.Errorf("bad input: %w", err)
			return err
		}

		// perform process
		result, err := h.Config.TransactionService.Find(ctx, transactionservice.FindParams{
			Limit:  reqparams.Limit,
			Offset: reqparams.Offset,
		})
		if err != nil {
			err = fmt.Errorf("listing failed: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{
			Results: result.Transactions,
			Total:   int32(result.Total),
			Limit:   int32(reqparams.Limit),
			Offset:  int32(reqparams.Offset),
		}
		return c.JSON(200, response.Normalize())
	}
}

func (h *Handlers) HandleSamplesCheckPopularRepo() echo.HandlerFunc {
	const AuthResourceName = "resgroup.common"

	type RequestParams struct{}

	type RequestData struct{}

	type ResponseData struct {
		Results []string `json:"results"`
	}

	return func(c echo.Context) error {
		var ctx = c.Request().Context()

		// prep and validate
		if err := ctxcontent.Authorize(ctx, "use", AuthResourceName); err != nil {
			ctxcontent.AddWarning(ctx, err.Error()) // always keep error

			resp := RespPresets[ErrUnauthorized]
			resp.Message += ": endpoint not allowed"
			return c.JSON(200, resp.Normalize())
		}

		// perform process
		result, err := h.Config.ClientGithubRepo.GetPopularRepoNames(ctx)
		if errors.Is(err, context.DeadlineExceeded) {
			ctxcontent.AddWarning(ctx, err.Error()) // always keep error

			resp := RespPresets[ErrProcessFailed]
			resp.Message += ": context deadline exceeded"
			return c.JSON(200, resp.Normalize())
		}
		if err != nil {
			err = fmt.Errorf("listing failed: %w", err)
			return err
		}

		// build response
		response := RespPresets[Success]
		response.Data = ResponseData{
			Results: result,
		}
		return c.JSON(200, response.Normalize())
	}
}
