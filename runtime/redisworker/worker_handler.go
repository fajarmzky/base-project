package redisworker

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/hibiken/asynq"
)

type HandlerDefault struct{ Config Config }

func (h *HandlerDefault) TriggerTransaction(ctx context.Context, t *asynq.Task) (err error) {
	// parse payload
	payload := transactionservice.TaskUpdateTransactionStatusInput{}
	err = json.Unmarshal(t.Payload(), &payload)
	if err != nil {
		err = fmt.Errorf("unparsable task payload: %w", err)
		return
	}

	// enrich context
	ctxcontent.SetData(ctx, "xid", payload.XID)

	// find transaction
	outFind, err := h.Config.TransactionService.Find(ctx, transactionservice.FindParams{}) // TODO: add id
	if err != nil {
		err = fmt.Errorf("find failed: %w", err)
		return
	}

	// update transaction
	trx := outFind.Transactions[0]
	trx.Status = payload.Status

	err = h.Config.TransactionService.Persist(ctx, trx) // TODO: add id
	if err != nil {
		err = fmt.Errorf("persist failed: %w", err)
		return
	}

	ctxcontent.CreateProcess(ctx, ctxcontent.Process{
		Name: "default",
		Data: map[string]interface{}{
			"final_trx": trx,
		},
	})

	return
}
