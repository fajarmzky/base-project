package redisworker

import (
	"context"
	"fmt"

	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/hibiken/asynq"
)

type Runtime interface {
	Run() (err error)
	Close() error
}

type Config struct {
	RootContext        context.Context            `validate:"required"`
	AsynqServer        *asynq.Server              `validate:"required"`
	TransactionService transactionservice.Service `validate:"required"`
}

var _ Runtime = &Worker{}

type Worker struct {
	config Config
}

func New(cfg Config) (*Worker, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	e := &Worker{config: cfg}

	return e, nil
}

func (e *Worker) Run() (err error) {
	// setup handlers
	handler := HandlerDefault{Config: e.config}

	// setup muxer
	mux := asynq.NewServeMux()
	mux.HandleFunc(e.Wrap(transactionservice.TaskUpdateTransactionStatusType, handler.TriggerTransaction))

	// start server
	if err = e.config.AsynqServer.Run(mux); err != nil {
		err = fmt.Errorf("cannot run server: %w", err)
		return
	}

	return
}

func (e *Worker) Close() error {
	return nil
}

func (e *Worker) Wrap(procname string, fx HandlerFunc) (string, HandlerFunc) {
	return procname, func(ctx context.Context, t *asynq.Task) error {
		// rootctx := e.config.RootContext
		// timestamp := time.Now()
		// procname := strings.ToLower(fmt.Sprintf("@redisworker/%s", procname))

		// // decorate context
		// svcinfo := ctxlocal.GetServiceInfo(rootctx)

		// ctx = ctxcontent.CreateWith(ctx)
		// ctx = stdlogger.InjectCtx(ctx, stdlogger.Context{
		// 	ServiceName:    svcinfo.ServiceID,
		// 	ServiceVersion: svcinfo.InfoVersion,
		// 	ServicePort:    svcinfo.InfoListenedPort,
		// 	ThreadID:       "", // context not propagated, track using params + ctxmonit
		// 	JourneyID:      "", // context not propagated, track using params + ctxmonit
		// 	ChainID:        "", // context not propagated, track using params + ctxmonit
		// 	Tag:            "instance_" + svcinfo.InstanceID,
		// 	ReqMethod:      "POST",
		// 	ReqURI:         procname,
		// })

		// // catch panics and logs on close
		// defer func() {
		// 	panicmsg := ""
		// 	if r := recover(); r != nil {
		// 		ctxcontent.AddVar(ctx, "panicked", true)
		// 		panicmsg = fmt.Sprintf("recovered from error: %v", r)
		// 	}

		// 	// write log
		// 	tdrdata := stdlogger.LogTdrModel{
		// 		AppName:      svcinfo.ServiceID,
		// 		AppVersion:   svcinfo.InfoVersion,
		// 		ThreadID:     "",
		// 		JourneyID:    "",
		// 		ChainID:      "",
		// 		Path:         procname,
		// 		Method:       "POST",
		// 		IP:           svcinfo.InfoLocalIPAddress,
		// 		Port:         svcinfo.InfoListenedPort,
		// 		SrcIP:        svcinfo.InfoLocalIPAddress,
		// 		RespTime:     int64(time.Since(timestamp).Milliseconds()),
		// 		ResponseCode: "200 OK",
		// 		Request:      "",
		// 		Error:        panicmsg,
		// 		AdditionalData: map[string]interface{}{
		// 			"path":      procname, // keep for monitoring
		// 			"vars":      ctxcontent.ListVars(ctx),
		// 			"warnings":  ctxcontent.ListWarnings(ctx),
		// 			"processes": ctxcontent.ListProcessesAsMap(ctx),
		// 		},
		// 		Header:   map[string]interface{}{},
		// 		Response: map[string]interface{}{},
		// 	}

		// 	logger.Instance().TDR(ctx, tdrdata)
		// 	logger.Instance().Debug(ctx, "redisworker task performed", logger.RemarshalToKVs(tdrdata)...)
		// }()

		// // perform original task
		// if err := fx(ctx, t); err != nil {
		// 	ctxcontent.AddWarning(ctx, err.Error())
		// 	err = nil // discard error
		// }

		return nil
	}
}

// ***

type HandlerFunc func(context.Context, *asynq.Task) error
