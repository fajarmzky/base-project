package cronworker

import (
	"context"

	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/robfig/cron/v3"
)

type Runtime interface {
	Run() (err error)
	Close() error
}

type Config struct {
	RootContext        context.Context            `validate:"required"`
	TransactionService transactionservice.Service `validate:"required"`
}

var _ Runtime = &Worker{}

type Worker struct {
	config Config
}

func New(cfg Config) (*Worker, error) {
	if err := validator.Validate(cfg); err != nil {
		return nil, err
	}

	e := &Worker{config: cfg}

	return e, nil
}

func (e *Worker) Run() (err error) {
	// setup engine
	c := cron.New()

	// setup handlers
	h := HandlerDefault{Config: e.config}
	c.AddFunc("@never", e.Wrap("cron-daily-cleanup", h.ExecuteDailyCleanup))
	c.AddFunc("@never", e.Wrap("cron-shortly-process", h.ExecuteShortlyProcess))

	// start engine
	c.Start()

	return
}

func (e *Worker) Close() error {
	return nil
}

func (e *Worker) Wrap(procname string, fx HandlerFunc) func() {
	return func() {
		// ctx := e.config.RootContext
		// timestamp := time.Now()
		// procname := strings.ToLower(fmt.Sprintf("@cron/%s", procname))

		// // decorate context
		// svcinfo := ctxlocal.GetServiceInfo(ctx)

		// ctx = ctxcontent.CreateWith(ctx)
		// ctx = stdlogger.InjectCtx(ctx, stdlogger.Context{
		// 	ServiceName:    svcinfo.ServiceID,
		// 	ServiceVersion: svcinfo.InfoVersion,
		// 	ServicePort:    svcinfo.InfoListenedPort,
		// 	ThreadID:       "", // context not propagated, track using params + ctxmonit
		// 	JourneyID:      "", // context not propagated, track using params + ctxmonit
		// 	ChainID:        "", // context not propagated, track using params + ctxmonit
		// 	Tag:            "instance_" + svcinfo.InstanceID,
		// 	ReqMethod:      "POST",
		// 	ReqURI:         procname,
		// })

		// catch panics and logs on close
		// defer func() {
		// 	panicmsg := ""
		// 	if r := recover(); r != nil {
		// 		ctxcontent.AddVar(ctx, "panicked", true)
		// 		panicmsg = fmt.Sprintf("recovered from error: %v", r)
		// 	}

		// 	// write log
		// 	tdrdata := stdlogger.LogTdrModel{
		// 		AppName:      svcinfo.ServiceID,
		// 		AppVersion:   svcinfo.InfoVersion,
		// 		ThreadID:     "",
		// 		JourneyID:    "",
		// 		ChainID:      "",
		// 		Path:         procname,
		// 		Method:       "POST",
		// 		IP:           svcinfo.InfoLocalIPAddress,
		// 		Port:         svcinfo.InfoListenedPort,
		// 		SrcIP:        svcinfo.InfoLocalIPAddress,
		// 		RespTime:     int64(time.Since(timestamp).Milliseconds()),
		// 		ResponseCode: "200 OK",
		// 		Request:      "",
		// 		Error:        panicmsg,
		// 		AdditionalData: map[string]interface{}{
		// 			"path":      procname, // keep for monitoring
		// 			"vars":      ctxcontent.ListVars(ctx),
		// 			"warnings":  ctxcontent.ListWarnings(ctx),
		// 			"processes": ctxcontent.ListProcessesAsMap(ctx),
		// 		},
		// 		Header:   map[string]interface{}{},
		// 		Response: map[string]interface{}{},
		// 	}

		// 	logger.Instance().TDR(ctx, tdrdata)
		// 	logger.Instance().Debug(ctx, "cron task performed", logger.RemarshalToKVs(tdrdata)...)
		// }()

		// perform original task
		// if err := fx(ctx); err != nil {
		// 	ctxcontent.AddWarning(ctx, err.Error())
		// 	err = nil // discard error
		// }
	}
}

// ***

type HandlerFunc func(context.Context) error
