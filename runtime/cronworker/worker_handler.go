package cronworker

import (
	"context"
)

type HandlerDefault struct{ Config Config }

func (h *HandlerDefault) ExecuteDailyCleanup(ctx context.Context) (err error) {
	return
}

func (h *HandlerDefault) ExecuteShortlyProcess(ctx context.Context) (err error) {
	return
}
