package cmd

import (
	"os"

	"github.com/go-playground/validator"
	"github.com/urfave/cli"
)

type Config struct{}

type Default struct {
	config  Config
	cmdRoot *cli.App
}

func New(cfg Config) (*Default, error) {
	if err := validator.New().Struct(cfg); err != nil {
		return nil, err
	}

	e := &Default{config: cfg}
	e.init()

	return e, nil
}

func (e *Default) init() {
	e.cmdRoot = &cli.App{
		Name:    "new service",
		Version: "v1",
		Usage:   "starter service",
		Action: func(c *cli.Context) error {
			return e.ExecDefault()
		},
		Commands: []cli.Command{
			{
				Name:  "server",
				Usage: "start application server",
				Action: func(c *cli.Context) error {
					return e.ExecDefault()
				},
			},
		},
	}

}

func (e *Default) Execute() error {
	return e.cmdRoot.Run(os.Args)
}
