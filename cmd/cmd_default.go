package cmd

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/fajarmzky/base-project/component/authorizer"
	"github.com/fajarmzky/base-project/component/sessionservice"
	"github.com/fajarmzky/base-project/component/sessionstore"
	"github.com/fajarmzky/base-project/component/transactionservice"
	"github.com/fajarmzky/base-project/component/transactionstore"
	"github.com/fajarmzky/base-project/component/userservice"
	"github.com/fajarmzky/base-project/component/userstore"
	"github.com/fajarmzky/base-project/pkg/clientgithubrepo"
	"github.com/fajarmzky/base-project/pkg/ctxcontent"
	"github.com/fajarmzky/base-project/pkg/validator"
	"github.com/fajarmzky/base-project/runtime/cronworker"
	"github.com/fajarmzky/base-project/runtime/httpserver"
	"github.com/fajarmzky/base-project/runtime/redisworker"
	"github.com/go-redis/redis/v8"
	"github.com/go-resty/resty/v2"
	"github.com/hibiken/asynq"
	"github.com/joho/godotenv"
	"github.com/rs/xid"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"gopkg.in/yaml.v2"
)

func (e *Default) ExecDefault() error {
	ctxRoot := context.Background()
	ctxRoot = ctxcontent.CreateWith(ctxRoot)

	type ConfigEnvironment struct {
		ConfigFilePath string `env:"CONFIG_FILE_PATH" validate:"required"`
		InstanceID     string `env:"INSTANCE_ID" validate:"-"`
	}

	var cfgEnv = ConfigEnvironment{
		ConfigFilePath: "config.yml",
		InstanceID:     "noid",
	}
	_ = godotenv.Load()
	if err := env.Parse(&cfgEnv); err != nil {
		err = fmt.Errorf("failure parsing environment file: %w", err)
		return err
	}
	if err := validator.Validate(cfgEnv); err != nil {
		err = fmt.Errorf("bad environment config: %w", err)
		return err
	}

	type ConfigFile struct {
		// interfaces
		Server struct {
			Port                              int  `yaml:"port" validate:"required"`
			RequestRateLimit                  int  `yaml:"request_rate_limit" validate:"-"`
			DebugEnableUnexpectedErrorMessage bool `yaml:"debug_enable_unexpected_error_message" validate:"-"`
		} `yaml:"server"`
		Authorization struct {
			SecretString      string            `yaml:"secret_string"`
			AccessDefinitions map[string]string `yaml:"access_definitions"`
		} `yaml:"authorization"`
		// dependencies
		Postgres struct {
			DSN             string        `yaml:"dsn" validate:"required"`
			MaxIdleConn     int           `yaml:"max_idle_conn" validate:"required"`
			MaxOpenConn     int           `yaml:"max_open_conn" validate:"required"`
			ConnMaxLifetime time.Duration `yaml:"conn_max_lifetime" validate:"required"`
		} `yaml:"postgres" validate:"required"`
		MongoDB struct {
			ConnectionURL string `yaml:"url" validate:"required"`
			DatabaseName  string `yaml:"db_name" validate:"required"`
		} `yaml:"mongodb"`
		Redis struct {
			Mode       string   `yaml:"mode" validate:"required,oneof=single sentinel cluster"`
			Address    []string `yaml:"address" validate:"required,unique,dive,required"`
			Username   string   `yaml:"username"`
			Password   string   `yaml:"password"`
			DB         int      `yaml:"db"`
			MasterName string   `yaml:"master_name" validate:"required_if=Mode sentinel"`
		} `yaml:"redis"`
		ClientGithubRepo struct {
			Enable                    bool          `yaml:"enable" validate:"-"`
			BaseUrl                   string        `yaml:"base_url" validate:"required"`
			ClientTimeout             time.Duration `yaml:"client_timeout" validate:"required"`
			Resiliency_HitThreshold   int           `yaml:"resiliency_hit_threshold" validate:"required"`
			Resiliency_MaxConsecFails int           `yaml:"resiliency_max_consec_fails" validate:"required"`
			Resiliency_MaxFailRate    float32       `yaml:"resiliency_max_fail_rate" validate:"required"`
		} `yaml:"clientgithubrepo"`
	}

	var cfgFile ConfigFile
	cfgContent, err := ioutil.ReadFile(cfgEnv.ConfigFilePath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(cfgContent, &cfgFile)
	if err != nil {
		return err
	}
	if err := validator.Validate(cfgFile); err != nil {
		err = fmt.Errorf("bad file config: %w", err)
		return err
	}

	// read current local ip
	// reference: https://stackoverflow.com/questions/23558425/how-do-i-get-the-local-ip-address-in-go
	ipaddr := "0.0.0.0"
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		conn = &net.UDPConn{}
	}
	defer conn.Close()
	if udpaddr, ok := conn.LocalAddr().(*net.UDPAddr); ok {
		ipaddr = udpaddr.IP.String()
	}
	log.Println(fmt.Sprintf("~ detected local ip address: %s", ipaddr))

	// =====================================================
	//    PREPARE DATA LAYER
	// =====================================================
	// prepare mongo db
	// ---
	log.Println("preparing storage layer")
	log.Println("~ preparing mongodb")
	ctxMongoConnect, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	mongoConn, err := mongo.Connect(ctxMongoConnect, options.Client().ApplyURI(cfgFile.MongoDB.ConnectionURL))
	if err != nil {
		err = fmt.Errorf("mongodb connect error: %w", err)
		return err
	}

	defer func() {
		ctxDisconnect, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err = mongoConn.Disconnect(ctxDisconnect); err != nil {
			err = fmt.Errorf("mongodb disconnect error: %w", err)
			return
		}
	}()

	// ensure mongodb is accessible before program really start
	ctxMongoPing, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err = mongoConn.Ping(ctxMongoPing, readpref.Primary()); err != nil {
		err = fmt.Errorf("mongodb ping error: %w", err)
		return err
	}

	mongoDefaultDB := mongoConn.Database(cfgFile.MongoDB.DatabaseName, options.Database().
		SetWriteConcern(writeconcern.New(writeconcern.WMajority())),
	)

	log.Printf("~ connected to database: %v\n", mongoDefaultDB.Name())

	// // setup postgre connection
	// // ---
	// log.Println("~ prepare postgres db connection")
	// dbPostgre, err := sql.Open("postgres", cfgFile.Postgres.DSN)
	// if err != nil {
	// 	err = fmt.Errorf("postgre connection failed: %w", err)
	// 	return err
	// }
	// defer func() {
	// 	if err := dbPostgre.Close(); err != nil {
	// 		err = fmt.Errorf("postgreclosing connection failure: %w", err)
	// 		log.Fatal(err)
	// 	}
	// }()

	// dbPostgre.SetMaxIdleConns(cfgFile.Postgres.MaxIdleConn)
	// dbPostgre.SetMaxOpenConns(cfgFile.Postgres.MaxOpenConn)
	// dbPostgre.SetConnMaxLifetime(cfgFile.Postgres.ConnMaxLifetime)

	// // ping database connection
	// if err = dbPostgre.Ping(); err != nil {
	// 	err = fmt.Errorf("cannot ping to database: %w", err)
	// 	return err
	// }

	// // setup gorm instance
	// // ---
	// log.Println("~ prepare postgres gorm db connection")
	// gormDB, err := gorm.Open(postgres.New(postgres.Config{Conn: dbPostgre}), &gorm.Config{
	// 	SkipDefaultTransaction: true,
	// })
	// if err != nil {
	// 	err = fmt.Errorf("gorm init failed: %w", err)
	// 	return err
	// }

	// prepare redis
	// ---
	log.Println(fmt.Sprintf("~ preparing redis connection mode %s", cfgFile.Redis.Mode))
	var redisClient redis.UniversalClient
	// Define each redis client based on "mode", this way we can explicitly states what infrastructure we use.
	// redis.NewUniversalClient is good, but lacking defining mode explicitly.
	switch cfgFile.Redis.Mode {
	case "single":
		redisClient = redis.NewClient(&redis.Options{
			Addr:     cfgFile.Redis.Address[0],
			Username: cfgFile.Redis.Username,
			Password: cfgFile.Redis.Password,
			DB:       cfgFile.Redis.DB,
		})
	case "sentinel":
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			SentinelAddrs: cfgFile.Redis.Address,
			Username:      cfgFile.Redis.Username,
			Password:      cfgFile.Redis.Password,
			DB:            cfgFile.Redis.DB,
			MasterName:    cfgFile.Redis.MasterName,
		})
	case "cluster":
		// cluster mode is not support DB selection
		redisClient = redis.NewClusterClient(&redis.ClusterOptions{
			Addrs:    cfgFile.Redis.Address,
			Username: cfgFile.Redis.Username,
			Password: cfgFile.Redis.Password,
		})
	default:
		err = fmt.Errorf("unknown redis mode: %s", cfgFile.Redis.Mode)
		log.Println(err)
		return err
	}

	ctxRedisPing, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if redisPing := redisClient.Ping(ctxRedisPing); redisPing.Err() != nil {
		err = fmt.Errorf("redis ping error: %w", redisPing.Err())
		log.Println(err)
		return err
	}

	// prepare asynq redis queue
	// ---
	// * prepare client
	asynqClient := asynq.NewClient(AsynqRedisClient{RedisClient: redisClient})
	defer asynqClient.Close()

	// * prepare server
	asynqServer := asynq.NewServer(AsynqRedisClient{RedisClient: redisClient},
		asynq.Config{
			Logger:          nil,
			LogLevel:        asynq.FatalLevel,
			Concurrency:     25, // TODO: extract as config
			ShutdownTimeout: 5 * time.Second,
		},
	)
	// defer asynqServer.Stop() // this shit messes up the logging system
	// =====================================================
	//    PREPARE EXTERNAL SERVICES CLIENTS
	// =====================================================
	log.Println("preparing external service clients")

	// prepare client pop repo
	// ---
	log.Println("~ preparing client pop repo")

	// prepare client
	var clientGithubRepo clientgithubrepo.Client
	clientGithubRepo, err = clientgithubrepo.NewHTTP(clientgithubrepo.ConfigHTTP{
		RESTClient:  resty.New(),
		HTTPTimeout: cfgFile.ClientGithubRepo.ClientTimeout,
		BaseURL:     cfgFile.ClientGithubRepo.BaseUrl,
	})
	if err != nil {
		err = fmt.Errorf("client pop repo init failed: %w", err)
		log.Println(err)
		return err
	}

	if !cfgFile.ClientGithubRepo.Enable {
		clientGithubRepo = clientgithubrepo.Dummy{}
	}

	// =====================================================
	//    PREPARE INTERNAL SERVICES
	// =====================================================
	log.Println("preparing internal service layer")

	// prepare authorizer
	// ---
	log.Println("~ preparing authorizer")
	authadapter, err := authorizer.NewAdapterLocal(authorizer.ConfigAdapterLocal{})
	if err != nil {
		err = fmt.Errorf("authorizer adapter init fail: %w", err)
		return err
	}
	// authadapter, err := authorizer.NewAdapterMongo(authorizer.ConfigAdapterMongo{
	// 	MongoClientOptions: options.Client().ApplyURI(cfgFile.MongoDB.ConnectionURL),
	// 	MongoDatabaseName:  cfgFile.MongoDB.DatabaseName,
	// })
	// if err != nil {
	// 	err = fmt.Errorf("authorizer adapter init fail: %w", err)
	// 	return err
	// }

	authr, err := authorizer.New(authorizer.Config{
		CasbinAdapter: authadapter,
		Policies: []authorizer.Policy{
			authorizer.PolicyAccess("superadmin", "resgroup.superadmin", "use"),
			authorizer.PolicyAccess("common", "resgroup.common", "use"),
			authorizer.PolicyAccess("core", "resgroup.core", "use"),
			authorizer.PolicyAccess("transaction", "resgroup.transaction", "use"),
			authorizer.PolicyAccess("sessionmanager", "resgroup.sessionmgmt", "use"),

			authorizer.PolicyRoleObject("res.sessions.create", "resgroup.sessionmgmt"),
			authorizer.PolicyRoleObject("res.sessions.refresh", "resgroup.sessionmgmt"),
			authorizer.PolicyRoleObject("res.sessions.expire", "resgroup.sessionmgmt"),

			// authorizer.PolicyRoleSubject("admin", "common"), // subject role
			// authorizer.PolicyRoleObject("ping", "resgroup.common"), // object role
		},
		EnableResourceGrouping: true,
	})
	if err != nil {
		err = fmt.Errorf("authorizer init fail: %w", err)
		return err
	}

	// add policies from local config
	// also build basic auth cred map
	basicauthCredMap := map[string]string{}
	localPolicies := []authorizer.Policy{}
	funcAccessDefToPolicy := func(rawcred, rawroles string) (out []authorizer.Policy, err error) {
		out = []authorizer.Policy{}

		splitcred := strings.SplitN(rawcred, ":", 2)
		if len(splitcred) != 2 {
			err = fmt.Errorf("bad credential definition")
			return
		}

		basicauthCredMap[splitcred[0]] = splitcred[1]

		splitroles := strings.Split(rawroles, ",")
		for _, r := range splitroles {
			out = append(out, authorizer.PolicyRoleSubject(splitcred[0], r))
		}

		return
	}

	for rawcred, rawroles := range cfgFile.Authorization.AccessDefinitions {
		policies, err := funcAccessDefToPolicy(rawcred, rawroles)
		if err != nil {
			err = fmt.Errorf("cannot parse policy: %w", err)
			return err
		}
		localPolicies = append(localPolicies, policies...)
	}

	err = authr.AddPolicies(context.Background(), localPolicies)
	if err != nil {
		err = fmt.Errorf("failure registering local policies: %w", err)
		return err
	}

	// prepare user service
	// ---
	log.Println("~ preparing users repository")
	userStore, err := userstore.NewMongoStore(userstore.ConfigMongo{
		MongoDB: mongoDefaultDB,
	})
	if err != nil {
		err = fmt.Errorf("users store init failed: %w", err)
		log.Println(err)
		return err
	}

	log.Println("~ preparing users service")
	userService, err := userservice.New(userservice.Config{
		Store:                   userStore,
		FuncPasswordAddedHasher: func(in string) (out string) { return in },
	})
	if err != nil {
		err = fmt.Errorf("users service init failed: %w", err)
		log.Println(err)
		return err
	}

	// prepare session service
	// ---
	log.Println("~ preparing session repository")
	sessStore, err := sessionstore.NewMongoStore(sessionstore.ConfigMongo{
		MongoDB: mongoDefaultDB,
	})
	if err != nil {
		err = fmt.Errorf("session store init failed: %w", err)
		log.Println(err)
		return err
	}

	log.Println("~ preparing session service")
	sessService, err := sessionservice.New(sessionservice.Config{
		Store:         sessStore,
		AccessExpiry:  time.Minute, // TODO: adjust expiry to change required refresh interval
		RefreshExpiry: time.Hour,   // TODO: adjust to change how long idle session ends
		FuncGenerateAccessToken: func(ctx context.Context, in sessionservice.InputCreate) (out string, err error) {
			out = xid.New().String() + xid.New().String() + xid.New().String()
			return
		},
	})
	if err != nil {
		err = fmt.Errorf("session service init failed: %w", err)
		log.Println(err)
		return err
	}

	// prepare transactions service
	// ---
	log.Println("~ preparing transactions repository")
	// trxRepo, err := transactionstore.NewPostgre(transactionstore.PostgreConfig{
	// 	GORM: gormDB,
	// })
	trxRepo, err := transactionstore.NewMongo(transactionstore.ConfigMongo{
		MongoDB: mongoDefaultDB,
	})
	if err != nil {
		err = fmt.Errorf("transaction store init failed: %w", err)
		log.Println(err)
		return err
	}

	log.Println("~ preparing transaction service")
	trxService, err := transactionservice.New(transactionservice.Config{
		Store: trxRepo,
		DelayedTaskPublisherFunc: func(taskname, taskid, payload string, processAt time.Time) (err error) {
			_, err = asynqClient.Enqueue(asynq.NewTask(taskname, []byte(payload)), asynq.ProcessAt(processAt))
			return
		},
	})
	if err != nil {
		err = fmt.Errorf("transaction service init failed: %w", err)
		log.Println(err)
		return err
	}
	// =====================================================
	//    PREPARE SYSTEM INTERFACES
	// =====================================================
	// prepare http server
	// ---
	log.Println("initializing http server")

	if cfgFile.Server.RequestRateLimit != 0 {
		log.Println(fmt.Sprintf("~ request rate limiter enabled: max %drps per ip", cfgFile.Server.RequestRateLimit))
	}

	// prepare server instance
	serverHTTP, err := httpserver.New(httpserver.Config{
		ServiceID:           e.cmdRoot.Name,
		InstanceID:          cfgEnv.InstanceID,
		InfoVersion:         e.cmdRoot.Version,
		InfoListenedPort:    cfgFile.Server.Port,
		InfoServerStartTime: time.Now(),
		InfoLocalIPAddress:  ipaddr,
		RootContext:         ctxRoot,

		EnableVerboseUnexpectedErrorResponse: cfgFile.Server.DebugEnableUnexpectedErrorMessage,
		RateLimiterMaxRequest:                cfgFile.Server.RequestRateLimit,
		RateLimiterRefreshInterval:           1 * time.Minute,
		LocalAuthSecretString:                cfgFile.Authorization.SecretString,
		LocalAuthCredMap:                     basicauthCredMap,

		Authorizer:         authr,
		UserService:        userService,
		SessionService:     sessService,
		TransactionService: trxService,
		ClientGithubRepo:   clientGithubRepo,
	})
	if err != nil {
		err = fmt.Errorf("server init failed: %w", err)
		return err
	}

	// prepare worker redis
	workerRedis, err := redisworker.New(redisworker.Config{
		RootContext:        ctxRoot,
		AsynqServer:        asynqServer,
		TransactionService: trxService,
	})
	if err != nil {
		err = fmt.Errorf("worker redis init failed: %w", err)
		return err
	}

	// prepare worker cron
	workerCron, err := cronworker.New(cronworker.Config{
		RootContext:        ctxRoot,
		TransactionService: trxService,
	})
	if err != nil {
		err = fmt.Errorf("worker cron init failed: %w", err)
		return err
	}

	// dispatch runtimes
	// ---
	log.Println("dispatching runtimes")
	// ** listen for sigterm signal
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// ** start server runtime
	log.Printf("~ using http://localhost:%d to start http server...\n", cfgFile.Server.Port)
	go func() {
		if err := serverHTTP.Run(); err != nil {
			err = fmt.Errorf("httpserver start error: %w", err)
			log.Fatal(err.Error())
		}
	}()

	// ** start worker redis runtime
	log.Printf("~ starting redis worker...\n")
	go func() {
		if err := workerRedis.Run(); err != nil {
			err = fmt.Errorf("redis worker startup error: %w", err)
			log.Fatal(err.Error())
		}
	}()

	// ** start worker cron runtime
	log.Printf("~ starting cron worker...\n")
	go func() {
		if err := workerCron.Run(); err != nil {
			err = fmt.Errorf("cron worker startup error: %w", err)
			log.Fatal(err.Error())
		}
	}()

	<-done // wait for sigterm and graceful shutdowns
	log.Println("sigterm caught, stopping runtimes...")

	log.Println("~ shutting down server...")
	if err := serverHTTP.Close(); err != nil {
		err = fmt.Errorf("http server shutdown error: %w", err)
		log.Println(err.Error())
	}

	log.Println("~ shutting down worker redis...")
	if err := workerRedis.Close(); err != nil {
		err = fmt.Errorf("worker redis shutdown error: %w", err)
		log.Println(err.Error())
	}

	log.Println("~ shutting down worker cron...")
	if err := workerCron.Close(); err != nil {
		err = fmt.Errorf("worker cron shutdown error: %w", err)
		log.Println(err.Error())
	}

	log.Println("~ exited")

	return nil
}

// ***

type AsynqRedisClient struct{ RedisClient redis.UniversalClient }

func (a AsynqRedisClient) MakeRedisClient() interface{} { return a.RedisClient }
